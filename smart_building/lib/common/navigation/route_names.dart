class RouteName {
  static const String initial = '/';
  static const String splashScreen = 'splashScreen';
  static const String loginScreen = 'loginScreen';
  static const String containerScreen = 'containerScreen';
  static const String upcomingEventScreen = 'upcomingEventScreen';
  static const String accountScreen = 'accountScreen';
  static const String detailServiceScreen = 'detailServiceScreen';
  static const String payScreen = 'payScreen';
  static const String paySuccessScreen = 'paySuccessScreen';
  static const String myQrScreen = 'myQrScreen';
  static const String billScreen = 'billScreen';
  static const String personalScreen = 'personalScreen';
  static const String rulesScreen = 'rulesScreen';
  static const String ruleDetailScreen = 'RuleDetailScreen';
  static const String notificationScreen = 'notificationScreen';
  static const String juridicalScreen = 'JuridicalScreen';
  static const String juridicalDetailScreen = 'JuridicalDetailScreen';
  static const String contractScreen = 'contractScreen';
  static const String contractDetailScreen = 'contractDetailScreen';

  static const String researchWidget = 'researchWidget';
}
