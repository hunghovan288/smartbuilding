import 'package:smart_building/common/bloc/snackbar_bloc/snackbar_state.dart';
import 'package:smart_building/presentation/widgets/snackbar_widget/enum.dart';

abstract class SnackbarEvent {}

class ShowSnackbarEvent extends SnackbarEvent {
  String content;
  SnackBarType type;

  ShowSnackbarEvent({
    this.content,
    this.type,
  });
}
