import 'package:flutter/foundation.dart';
import 'package:smart_building/presentation/widgets/snackbar_widget/enum.dart';

abstract class SnackbarState {
  final String content;
  final SnackBarType type;

  SnackbarState({
    this.content,
    this.type,
  });
}

class InitialSnackbarState extends SnackbarState {}



class ShowSnackBarState extends SnackbarState {
  ShowSnackBarState({
    @required type,
    String content,
  }) : super(
          type: type,
          content: content ?? '',
        );
}
