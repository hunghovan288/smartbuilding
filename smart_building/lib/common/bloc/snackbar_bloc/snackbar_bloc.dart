import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_building/common/bloc/snackbar_bloc/snackbar_event.dart';
import 'package:smart_building/common/bloc/snackbar_bloc/snackbar_state.dart';

class SnackBarBloc extends Bloc<SnackbarEvent, SnackbarState> {
  SnackBarBloc() : super(InitialSnackbarState());

  @override
  Stream<SnackbarState> mapEventToState(SnackbarEvent event) async* {
    if (event is ShowSnackbarEvent) {
      yield ShowSnackBarState(
        content: event.content,
        type: event.type,
      );
    }
  }
}
