import 'package:permission_handler/permission_handler.dart';
import 'package:smart_building/common/utils/log_util.dart';

class PermissionUtil {
  static Future<bool> isPermissionGranted(Permission permission) async {
    final status = await permission.request();
    LOG.d('PermissionUtil: $status');
    return status.isGranted;
  }
}
