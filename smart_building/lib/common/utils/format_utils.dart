import 'package:intl/intl.dart';

class FormatUtils {
  static String formatDateTimeToString(String formatType, DateTime dateTime) {
    DateFormat dateFormat = DateFormat(formatType);
    return dateFormat.format(dateTime);
  }
  static String formatCurrencyDoubleToString(double currency,
      {bool haveUnit = true}) {
    try {
      if (currency == null) return '0${haveUnit ? ' đ' : ''}';
      final output = NumberFormat.simpleCurrency(locale: 'vi').format(currency);
      return haveUnit ? output : output.trim().replaceAll('₫', '');
    } catch (e) {
      return '$currency';
    }
  }

}
