extension ListExtention on List{
  bool get isEmptyOrNull {
    if (this == null) {
      return true;
    }
    return isEmpty;
  }

  bool get isNotEmptyAndNull => this != null && isNotEmpty;
}