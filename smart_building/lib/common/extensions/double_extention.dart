extension DoubleExtention on double {
  bool get zeroOrNull => this == null || this == 0.0;
}
