import 'package:smart_building/common/constant/regex_constants.dart';

const _chars =
    'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890!@#\$%^&*';

extension StringExtension on String {
  bool get isEmptyOrNull {
    if (this == null) {
      return true;
    }
    return isEmpty;
  }

  bool get isNotEmptyAndNull => this != null && isNotEmpty;

  bool get isValidEmailString =>
      this != null && RegExp(RegexConstants.validEmailRegex).hasMatch(trim());

  bool get isValidPhoneString =>
      this != null && RegExp(RegexConstants.validPhoneRegex).hasMatch(trim());

  bool get isValidPassword => isEmptyOrNull && length > 6;

  double toPrice() =>
      double.parse(replaceAll(RegExp(RegexConstants.notDigitRegex), ''));

  double toDouble() =>
      double.parse(this.substring(0, this.length - 2).split('.').join());

  String get toFirstUpperCase {
    return '${this[0].toUpperCase()}${this.substring(1)}';
  }

  String get toUpperCaseFirstChar {
    if (this == null || this.isEmpty) {
      return '';
    }
    final listWord = this.trim().split(' ');
    final resultWord = [];
    for (final obj in listWord) {
      resultWord.add('${obj[0].toUpperCase()}${obj.substring(1)}');
    }
    return resultWord.join(' ');
  }

}
