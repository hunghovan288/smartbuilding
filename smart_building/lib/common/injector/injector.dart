import 'package:kiwi/kiwi.dart';
import 'package:smart_building/common/bloc/loading_bloc/loading_bloc.dart';
import 'package:smart_building/common/bloc/snackbar_bloc/snackbar_bloc.dart';
import 'package:smart_building/presentation/journey/container/bloc/container_bloc.dart';

part 'injector.g.dart';

abstract class Injector {
  static KiwiContainer container;

  static void setup() {
    container = KiwiContainer();
    _$Injector()._configure();
  }

  static final resolve = container.resolve;

  void _configure() {
    _configureBlocs();
    _configureUseCases();
    _configureRepositories();
    _configureRemoteDataSources();
    _configureLocalDataSources();
    _configureCommon();
  }

  void _configureBlocs();

  void _configureUseCases();

  void _configureRepositories();

  void _configureRemoteDataSources();

  void _configureLocalDataSources();

  void _configureCommon();
}
