part of 'injector.dart';

class _$Injector extends Injector {
  void _configureBlocs() {
    final KiwiContainer container = KiwiContainer();
    container.registerSingleton((c) => LoadingBloc());
    container.registerSingleton((c) => SnackBarBloc());
    container.registerSingleton((c) => ContainerBloc());
  }

  void _configureUseCases() {}

  void _configureRepositories() {}

  void _configureRemoteDataSources() {}

  void _configureLocalDataSources() {
  }

  void _configureCommon() {}
}
