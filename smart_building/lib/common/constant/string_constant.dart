class StringConstant {
  static const String _path = 'StringConstant.';

  static const String bill = '${_path}bill';
  static const String more = '${_path}more';
  static const String pay = '${_path}pay';
  static const String comingSoon = '${_path}comingSoon';
  static const String title = '${_path}title';
  static const String content = '${_path}content';

  static const String companyName = 'Công ty Cổ Phần Hưng Phú Invest';
}
