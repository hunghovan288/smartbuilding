import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/data/models/bill_model.dart';
import 'package:smart_building/data/models/card_model.dart';
import 'package:smart_building/data/models/complain_model.dart';
import 'package:smart_building/data/models/equipment_model.dart';
import 'package:smart_building/data/models/event_model.dart';
import 'package:smart_building/data/models/notification_model.dart';
import 'package:smart_building/data/models/room_model.dart';
import 'package:smart_building/data/models/service_model.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';

class MockConstant {
  static const String _pathIcon = 'assets/mock/';
  static const String icon1 = '${_pathIcon}icon1.svg';
  static const String icon2 = '${_pathIcon}icon2.svg';
  static const String icon3 = '${_pathIcon}icon3.svg';
  static const String icon4 = '${_pathIcon}icon4.svg';
  static const String pdf = '${_pathIcon}pdf.svg';
  static const String detailContract = '${_pathIcon}detail_contract.png';
  static const String detailJuridical = '${_pathIcon}detail_juridical.png';
  static const String urlAvatar =
      'https://thethaiger.com/wp-content/uploads/2020/04/hoang-thuy-linh-1-1150-1586055948.jpg';

  static List<NotificationModel> mockNotification() => [
        NotificationModel(
          id: 1,
          icon: MockConstant.icon1,
          content:
              'Kính gửi các cư dân tại tòa ABC chung cư FFF, ban quản lí tòa nhà xin thông báo về lịch diễn tập Phòng cháy chữa cháy tại tòa nhà vào ngày 31/2/2020...',
          title: 'Thông báo về lịch diễn tập Phòng cháy chữa cháy.',
        ),
        NotificationModel(
          id: 2,
          icon: MockConstant.icon2,
          title: 'Thông báo lịch đăng kí tạm trú tạm vắng',
          content:
              'Kính gửi các cư dân tại tòa ABC chung cư FFF, ban quản lí tòa nhà xin thông báo về thời hạn đăng kí...',
        ),
        NotificationModel(
          id: 3,
          icon: MockConstant.icon3,
          title: 'Thanh toán tiền điện tháng 10/2020',
          content:
              'Hóa đơn tiền điện tháng 10/2020 của bạn đã được thanh toán thành công ! Chúc cư dân một tháng mới đầy năng lượng !',
        ),
        NotificationModel(
          id: 4,
          icon: MockConstant.icon4,
          title: 'Thanh toán tiền nước tháng 10/2020',
          content:
              'Hóa đơn tiền nước tháng 10/2020 của bạn đã được thanh toán thành công ! Chúc cư dân một tháng mới đầy năng lượng !',
        ),
      ];

  static List<ServiceModel> mockServices() => [
        ServiceModel(
            id: 1,
            color: AppColors.catCleanHouse,
            icon: IconConstant.serviceCleanHouse,
            text: DashBoardConstant.serviceCleanHouse),
        ServiceModel(
            id: 2,
            color: AppColors.catWatchHouse,
            icon: IconConstant.serviceWatchHouse,
            text: DashBoardConstant.serviceWatchHouse),
        ServiceModel(
            id: 3,
            color: AppColors.catGarden,
            icon: IconConstant.serviceGarden,
            text: DashBoardConstant.serviceGarden),
        ServiceModel(
            id: 4,
            color: AppColors.catLaundry,
            icon: IconConstant.serviceLaundry,
            text: DashBoardConstant.serviceLaundry),
        ServiceModel(
            id: 5,
            color: AppColors.catRepair,
            icon: IconConstant.serviceRepair,
            text: DashBoardConstant.serviceRepair),
        ServiceModel(
            id: 6,
            color: AppColors.catPet,
            icon: IconConstant.servicePet,
            text: DashBoardConstant.servicePet),
        ServiceModel(
            id: 7,
            color: AppColors.catDelivery,
            icon: IconConstant.serviceDelivery,
            text: DashBoardConstant.serviceDelivery),
        ServiceModel(
            id: 8,
            color: AppColors.catSurvey,
            icon: IconConstant.serviceSurvey,
            text: DashBoardConstant.serviceSurvey),
        ServiceModel(
            id: 9,
            color: AppColors.catMap,
            icon: IconConstant.serviceMap,
            text: DashBoardConstant.serviceMap),
        ServiceModel(
            id: 10,
            color: AppColors.catEnv,
            icon: IconConstant.serviceEnv,
            text: DashBoardConstant.serviceEnv),
        ServiceModel(
            id: 11,
            color: AppColors.catCamera,
            icon: IconConstant.serviceCamera,
            text: DashBoardConstant.serviceCamera),
        ServiceModel(
            id: 12,
            color: AppColors.catBill,
            icon: IconConstant.serviceBill,
            text: DashBoardConstant.serviceBill),
        ServiceModel(
            id: 13,
            color: AppColors.catBill,
            icon: IconConstant.serviceBill,
            text: 'Item 1'),
        ServiceModel(
            id: 14,
            color: AppColors.catBill,
            icon: IconConstant.serviceBill,
            text: 'Item 2'),
        ServiceModel(
            id: 15,
            color: AppColors.catBill,
            icon: IconConstant.serviceBill,
            text: 'Item 3'),
        ServiceModel(
            id: 16,
            color: AppColors.catBill,
            icon: IconConstant.serviceBill,
            text: 'Item 4'),
      ];
  static List<ComplainModel> mockComplain = [
    ComplainModel(date: '14/02/2021', title: 'Khiếu nại về an ninh', status: 1),
    ComplainModel(
        date: '14/02/2021', title: 'Khiếu nại về tiếng ồn', status: 2),
    ComplainModel(
        date: '14/02/2021', title: 'Hoá đơn tiền điện tháng 2', status: 1),
    ComplainModel(
        date: '14/02/2021', title: 'Hoá đơn tiền điện tháng 2', status: 2),
  ];

  static List<BillModel> mockBill = [
    BillModel(date: '14/02/2021', title: 'Hoá đơn tiền điện tháng 2'),
    BillModel(
        date: '14/02/2021',
        title: 'Thanh toán tiền dịch vụ smart home tháng 2'),
    BillModel(date: '14/02/2021', title: 'Hoá đơn tiền điện tháng 2'),
    BillModel(date: '14/02/2021', title: 'Hoá đơn tiền điện tháng 2'),
  ];

  static List<EventModel> mockEvent = [
    EventModel(
        name: 'Khai mạc tuần lễ công nghệ',
        icon:
            'https://www.nachhaltigkeitsrat.de/wp-content/uploads/2019/06/20190604_RNE_Jahreskonferenz_DSH1445-1400x933.jpg'),
    EventModel(
        name: 'Lễ trao thưởng các dự án phần mềm thông minh',
        icon:
            'https://aeonmall-binhduongcanary-en.com/wp-content/uploads/2018/12/Slide1.jpg'),
    EventModel(
        name: 'Lễ trao thưởng các dự án phần mềm thông minh',
        icon:
            'https://aeonmall-binhduongcanary-en.com/wp-content/uploads/2018/12/Slide1.jpg'),
    EventModel(
        name: 'Lễ trao thưởng các dự án phần mềm thông minh',
        icon:
            'https://aeonmall-binhduongcanary-en.com/wp-content/uploads/2018/12/Slide1.jpg'),
  ];

  static List<CardModel> mockCards = [
    CardModel(
        name: 'Thẻ ra vào',
        id: '12345772',
        enable: true,
        place: 'Tòa B1 - P102',
        type: 1),
    CardModel(
        name: 'Thẻ phòng gym',
        id: '12345723',
        enable: true,
        place: 'Khu dịch vụ',
        type: 2),
  ];
  static List<RoomModel> mockRooms = [
    RoomModel(
        name: 'Phòng ngủ',
        url:
            'https://wedo.vn/wp-content/uploads/2018/08/thiet-ke-noi-that-phong-ngu-15m2-dep-1.jpg'),
    RoomModel(
        name: 'Phòng bếp',
        url:
            'https://wedo.vn/wp-content/uploads/2018/08/thiet-ke-noi-that-phong-ngu-15m2-dep-5.jpg'),
  ];
  static List<EquipmentModel> mockEquipment = [
    EquipmentModel(
      name: 'Đèn 1',
      enable: true,
    ),
    EquipmentModel(
      name: 'Đèn 2',
      enable: true,
    ),
    EquipmentModel(
      name: 'Đèn 3',
      enable: false,
    ),
    EquipmentModel(
      name: 'Đèn 4',
      enable: true,
    ),
    EquipmentModel(
      name: 'Đèn 5',
      enable: false,
    ),
    EquipmentModel(
      name: 'Đèn 6',
      enable: true,
    ),
    EquipmentModel(
      name: 'Đèn 7 ',
      enable: false,
    ),
  ];
  static const String mockDetailRule =
      '1. Nghiêm cấm chiếm dụng nhà ở trái pháp luật, tự ý thay đổi kết cấu'
      ' chịu lực, kiến trúc, điện nước hoặc thay đổi kết cấu căn hộ trong '
      'tòa nhà.\n\n2. Nghiêm cấm cơi nới làm lồng sắt, mái tôn, mái vẩy khu vực '
      'ban công dưới mọi hình thức.\n\n3. Nghiêm cấm tháo dỡ hoặc di dời làm hư'
      ' hỏng các thiết bị kỹ thuật đã lắp đặt tại tòa nhà.\n\n4. Nghiêm cấm quảng '
      'cáo, viết, vẽ trái quy định phía mặt ngoài căn hộ, khu công cộng.\n\n5. '
      'Nghiêm cấm mang chất độc hại dễ cháy nổ như xăng, dầu, bếp than, vũ'
      ' khí, đạn dược, chất cấm… vào tòa nhà.\n\n6. Không vứt rác, phóng '
      'uế tại khu hành lang, ban công, thang máy, thang bộ. (Yêu cầu tự vận '
      'chuyển rác thải sinh hoạt đến khu vực tập kết đã quy định).\n\n7. Không '
      'hóa vàng (đốt giấy tiền, đốt mã…) trong căn hộ, hành lang thuộc tòa nhà. '
      '(Yêu cầu tập kết hóa vàng. đốt mã đúng nơi quy định).\n\n8. Không bỏ '
      'vào toilet, hố ga những đồ vật khó phân hủy hoặc làm tắc nghẽn các '
      'đường ống thoát chung.\n\n9. Giữ gìn trật tự nơi công cộng, không gây '
      'tiếng ồn quá mức làm ảnh hưởng đến sinh hoạt của cư dân sinh sống trong '
      'tòa nhà.\n\n10. Đỗ xe đúng nơi quy định. (Yêu cầu đăng ký gửi xe, '
      'đăng ký vé tháng với Ban quản lý tòa nhà).\n\n11. Khách đến tòa nhà '
      'phải có trách nhiệm đăng ký với Ban quản lý và làm thủ tục tạm trú, '
      'tạm vắng theo quy định của Pháp luật.\n\n12. Mọi cá nhân, tổ chức sinh '
      'sống và làm việc tại toà nhà phải tuân thủ những quy định về an toàn '
      'phòng chống cháy nổ, có trách nhiệm tham gia với lực lượng bảo vệ tòa '
      'nhà và lực lượng PCCC để ngăn chặn và xử lý kịp thời khi có sự cố xảy '
      'ra.\n\n13. Mọi cư dẫn có trách nhiệm giữ gìn và bảo quản tài sản sở hữu '
      'chung, nếu làm hư hỏng phải có trách nhiệm bồi thường thiệt hại theo quy định.'
      '\n\n14. Mọi cư dân phải có trách nhiệm thực hiện theo đúng nội quy chung'
      ' cư, tòa nhà. Trường hợp cố tình vi phạm Ban quản lý tòa nhà sẽ từ chối '
      'cung cấp dịch vụ và xử lý theo Pháp luật';
}
