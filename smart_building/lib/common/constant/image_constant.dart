class ImageConstant{
  static const String path = 'assets/images/';
  static const String paySuccessBackground3 = '${path}pay_success.png';
  // splash
  static const String backgroundSplash = '${path}splash_background_2.png';

  // home
  static const String backgroundTopHome = '${path}dashboard_background.png';
}