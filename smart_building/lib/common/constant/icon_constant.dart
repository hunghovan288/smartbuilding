class IconConstant {
  static const String path = 'assets/icons/';
  static const String iconClose = '${path}cancel.svg';
  static const String imagePlaceHolder = '${path}image_place_holder.svg';
  static const String back = '${path}back.svg';
  static const String qrCode = '${path}icon_qr.svg';
  static const String right = '${path}right.svg';
  static const String person = '${path}person.svg';

  // login screen
  static const String profile = '${path}profile.svg';
  static const String lock = '${path}lock.svg';

  // dashboard screen
  static const String dashboardComplain = '${path}dashboard_complain.png';
  static const String dashboardCurrentLocation =
      '${path}dashboard_current_location.png';
  static const String dashboardFace = '${path}dashboard_face.png';
  static const String dashboardCall = '${path}dashboard_calling.png';
  static const String dashboardWallet = '${path}dashboard_wallet.png';
  static const String dashboardBell = '${path}bell.svg';

  // service
  static const String serviceBill = '${path}service_bill.svg';
  static const String serviceCamera = '${path}service_camera.svg';
  static const String serviceEnv = '${path}service_env.svg';
  static const String serviceMap = '${path}service_map.svg';
  static const String serviceCleanHouse = '${path}service_clean_house.svg';
  static const String serviceDelivery = '${path}service_delivery.svg';
  static const String serviceGarden = '${path}service_garden.svg';
  static const String serviceLaundry = '${path}service_laundry.svg';
  static const String servicePet = '${path}service_pet.svg';
  static const String serviceRepair = '${path}service_repair.svg';
  static const String serviceSurvey = '${path}service_survey.svg';
  static const String serviceWatchHouse = '${path}service_watch_house.svg';

  // complain screen
  static const String complainAdd = '${path}complain_add.svg';
  static const String complainDone = '${path}complain_done.svg';
  static const String complainHandling = '${path}complain_handling.svg';

  // service detail screen
  static const String serviceDetailLock = '${path}service_detail_lock.svg';

  // home screen
  static const String homeElectricPower = '${path}electric_power.svg';
  static const String homeHumidity = '${path}humidity.svg';
  static const String homeTemperature = '${path}temperature.svg';
  static const String homeLight = '${path}home_light.svg';
  static const String homeRightAll = '${path}home_right_all.svg';

  // pay success screen
  static const String paySuccessDone = '${path}pay_success_done.svg';

  // my QR screen
  static const String myQrCode = '${path}qr_code.svg';
  static const String myQrbarCode = '${path}barcode.svg';
  static const String myQrLight = '${path}myqr_light.svg';

  // personal screen
  static const String personalSetting = '${path}personal_setting.svg';
  static const String personalAccount = '${path}personal_account.svg';
  static const String personalRules = '${path}personal_rules.svg';
  static const String personalContract = '${path}personal_contract.svg';
  static const String personalJuridical = '${path}personal_juridical.svg';

  // snackbar
  static const String snackbarSuccess = '${path}snackbar_success.svg';
  static const String snackbarWarning = '${path}snackbar_warning.svg';
  static const String snackbarFail = '${path}snackbar_failed.svg';
}
