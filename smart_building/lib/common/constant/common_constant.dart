import 'dart:math';

import 'package:flutter/material.dart';

class CommonConstant {
  static const double defaultHeightKeyBoard = 175;

  static List<BoxShadow> defaultShadow = [
    BoxShadow(
        color: Colors.black.withOpacity(0.1), spreadRadius: 3, blurRadius: 5)
  ];
  static List<BoxShadow> primaryShadow = [
    BoxShadow(
        color: Color(0xff714FFF).withOpacity(0.15),
        spreadRadius: 3,
        blurRadius: 5)
  ];
  static List<Color> gradientPrimary = [
    Color(0xff5840BB),
    Color(0xff332C6B),
  ];
  static Paint gradientText = (Paint()
    ..shader = LinearGradient(
      colors: <Color>[Color(0xffDA44bb), Color(0xff8921aa)],
    ).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0)));

  static const double heightItemWidget66 = 66.0;

  //  type when select time
  static const int TYPE_TIME_WEEK = 3;
  static const int TYPE_TIME_MONTH = 2;
  static const int TYPE_TIME_YEAR = 1;
}
