class ColumnChartModel {
  int textTime;
  int total;
  int id;
  ColumnChartModel({this.textTime, this.total,this.id});

  ColumnChartModel.fromJson(Map<String, dynamic> json) {
    textTime = json['I'];
    total = json['Total'];
  }
}
