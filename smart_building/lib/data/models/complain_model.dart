class ComplainModel {
  final String title;
  final String date;
  final int status;

  ComplainModel({
    this.title,
    this.date,
    this.status,
  });
}
