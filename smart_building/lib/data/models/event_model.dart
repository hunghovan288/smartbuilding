class EventModel{
  final String name;
  final String icon;

  EventModel({this.name, this.icon});
}