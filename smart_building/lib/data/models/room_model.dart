class RoomModel{
  final String name;
  final String url;

  RoomModel({this.name, this.url});
}