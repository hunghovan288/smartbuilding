class NotificationModel {
  final int id;
  final String icon;
  final String title;
  final String content;

  NotificationModel({
    this.id,
    this.icon,
    this.title,
    this.content,
  });
}
