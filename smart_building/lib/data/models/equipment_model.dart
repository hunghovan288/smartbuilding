class EquipmentModel{
  final String name;
  final bool enable;

  EquipmentModel({this.name, this.enable});
}