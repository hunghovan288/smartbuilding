import 'package:flutter/material.dart';

class ServiceModel{
  final int id;
  final String icon;
  final Color color;
  final String text;

  ServiceModel({this.id, this.icon, this.color, this.text});
}