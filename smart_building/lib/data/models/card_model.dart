class CardModel {
  final String id;
  final String place;
  final String name;
    bool enable;
  final int type;

  CardModel({this.id, this.place, this.name, this.enable, this.type});
}
