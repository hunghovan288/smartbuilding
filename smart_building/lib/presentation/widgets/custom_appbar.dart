import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

const double defaultAppbar = 56.0;

class CustomAppBar extends StatelessWidget {
  final Function onBack;
  final Widget widgetRight;
  final String title;
  final Color colorIconBack;
  final Color backgroundColor;
  final bool gradient;

  const CustomAppBar({
    Key key,
    this.onBack,
    this.colorIconBack,
    this.backgroundColor,
    this.widgetRight,
    this.gradient = true,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(top: ScreenUtil.statusBarHeight),
      height: defaultAppbar + ScreenUtil.statusBarHeight,
      decoration: BoxDecoration(
        color: backgroundColor ?? AppColors.white,
        gradient: gradient
            ? LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                    Color(0xff5840BB),
                    Color(0xff352E70),
                  ])
            : null,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          onBack != null
              ? InkWell(
                  onTap: onBack,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: PixelConstant.marginHorizontal),
                    child: Container(
                      width: 48,
                      height: 48,
                      padding: EdgeInsets.only(top: 16, bottom: 16, right: 22),
                      child: SvgPicture.asset(
                        IconConstant.back,
                        color: colorIconBack ?? AppColors.white,
                      ),
                    ),
                  ),
                )
              : const SizedBox(),
          Expanded(
              child: Text(
            title ?? '',
            style: AppTextTheme.bold16PxW700.copyWith(
              color: AppColors.white,
              fontWeight: FontWeight.w500,
            ),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
          )),
          widgetRight ??
              SizedBox(
                width: onBack != null ? 64 : 0,
              ),
        ],
      ),
    );
  }
}
