import 'package:flutter/material.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController controller;
  final Function(String) onValidate;
  final int maxLine;
  final Key formKey;
  final String hintText;
  final TextStyle style;
  final Function(String) onChange;
  final TextInputAction textInputAction;
  final TextInputType textInputType;
  final double contentPaddingHorizontal;
  final double contentPaddingVertical;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final bool readOnly;
  final FocusNode focusNode;
  final EdgeInsetsGeometry contentPadding;
  final TextCapitalization textCapitalization;
  final bool isPassword;

  CustomTextField(
      {this.controller,
      this.onValidate,
      this.focusNode,
      this.style,
      this.isPassword,
      this.textInputType,
      this.suffixIcon,
      this.prefixIcon,
      this.formKey,
      this.maxLine,
      this.readOnly = false,
      this.textInputAction,
      this.hintText = '',
      this.onChange,
      this.contentPadding,
      this.contentPaddingHorizontal,
      this.contentPaddingVertical,
      this.textCapitalization});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 56,
      child: Column(
        children: [
          Expanded(
            child: TextFormField(
              onChanged: onChange,
              obscureText: isPassword ?? false,
              readOnly: readOnly,
              controller: controller,
              validator: onValidate,
              focusNode: focusNode,
              style: style ?? AppTextTheme.normalPrimary,
              keyboardType: textInputType,
              textInputAction: textInputAction ?? TextInputAction.newline,
              decoration: InputDecoration(
                hintText: hintText,
                filled: true,
                suffixIcon: suffixIcon,
                prefixIcon: prefixIcon,
                fillColor: AppColors.white,
                hintStyle: AppTextTheme.normalGrey,
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                contentPadding: contentPadding ??
                    EdgeInsets.only(top: 19, bottom: 15, left: 16, right: 16),
              ),
              textCapitalization: textCapitalization ?? TextCapitalization.none,
              maxLines: isPassword == true ? 1 : maxLine,
            ),
          ),
          const Divider(
            height: 1.5,
            color: AppColors.grey6,
          )
        ],
      ),
    );
  }
}
