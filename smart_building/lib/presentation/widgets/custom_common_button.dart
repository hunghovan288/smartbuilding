import 'dart:math';

import 'package:flutter/material.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/extensions/screen_extension.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class CustomCommonButton extends StatelessWidget {
  final double padding;
  final String text;
  final Function onTap;

  const CustomCommonButton({Key key, this.padding, this.text, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: padding ?? 0),
        height: max(40.h, 48),
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: CommonConstant.gradientPrimary),
            borderRadius: BorderRadius.circular(max(40.h, 48) / 2)),
        child: Center(
          child: Text(
            text ?? '',
            style: AppTextTheme.mediumBlack
                .copyWith(color: AppColors.white, fontWeight: FontWeight.w700),
          ),
        ),
      ),
    );
  }
}
