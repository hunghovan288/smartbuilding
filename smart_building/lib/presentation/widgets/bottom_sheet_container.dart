import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class BottomSheetContainer extends StatelessWidget {
  final Widget child;
  final String title;
  final Function onClose;
  final Function onRightTap;
  final String textButtonRight;
  final Color backgroundColor;

  const BottomSheetContainer({
    Key key,
    this.child,
    this.title,
    this.onClose,
    this.backgroundColor,
    this.onRightTap,
    this.textButtonRight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
        ),
        color: backgroundColor ?? Colors.white,
      ),
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 56,
            decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(12),
                    topLeft: Radius.circular(12))),
            child: Row(
              children: [
                InkWell(
                  onTap: onClose,
                  child: Container(
                    width: 56,
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(12),
                      ),
                    ),
                    child: Center(
                        child: SvgPicture.asset(
                      IconConstant.iconClose,
                      width: 16,
                      height: 16,
                      color: AppColors.grey9,
                    )),
                  ),
                ),
                Expanded(
                  child: Text(
                    title ?? '',
                    style: AppTextTheme.mediumBlack
                        .copyWith(fontWeight: FontWeight.w500),
                    textAlign: TextAlign.center,
                  ),
                ),
                InkWell(
                  onTap: onRightTap,
                  child: Container(
                    width: 56,
                    height: 60,
                    margin: EdgeInsets.only(right: 4),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(12),
                      ),
                    ),
                    child: Center(
                        child: Text(
                      textButtonRight ?? '',
                      style: AppTextTheme.normalBlue,
                    )),
                  ),
                )
              ],
            ),
          ),
          Divider(height: 1, color: AppColors.grey5),
          Expanded(child: child),
        ],
      ),
    );
  }
}
