import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shimmer/shimmer.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';

class CustomImageNetwork extends StatelessWidget {
  final String url;
  final double width;
  final double height;
  final BoxFit fit;
  final double border;
  final BorderRadius borderRadius;

  const CustomImageNetwork({
    Key key,
    @required this.url,
    this.width = 40,
    this.height = 40,
    this.fit,
    this.borderRadius,
    this.border,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if ((url ?? '').isEmpty) {
      return _widgetImagePlaceHolder();
    }
    if (url.contains('http')) {
      return ClipRRect(
        borderRadius:
            borderRadius ?? BorderRadius.all(Radius.circular(border ?? 0)),
        child: Image.network(
          url,
          fit: fit,
          width: width,
          height: height,
          errorBuilder: (
            BuildContext context,
            Object error,
            StackTrace stackTrace,
          ) {
            return _widgetImagePlaceHolder();
          },
          loadingBuilder: (
            BuildContext context,
            Widget child,
            ImageChunkEvent loadingProgress,
          ) {
            if (loadingProgress == null) {
              return child;
            }
            return Shimmer.fromColors(
              baseColor: Colors.grey[300],
              highlightColor: Colors.grey[100],
              enabled: true,
              child: Container(
                width: width,
                height: height,
                color: AppColors.white,
              ),
            );
          },
        ),
      );
    }
    return _widgetImagePlaceHolder();
  }

  Widget _widgetImagePlaceHolder() => Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          border: Border.all(color: AppColors.grey4),
        ),
        padding: EdgeInsets.all(16),
        child: Center(
          child: SvgPicture.asset(
            IconConstant.imagePlaceHolder,
            width: double.infinity,
            height: double.infinity,
            color: AppColors.grey4,
          ),
        ),
      );
}
