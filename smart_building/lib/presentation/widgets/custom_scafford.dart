import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smart_building/common/utils/common_util.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';

const double defaultAppbar = 56.0;

class CustomScaffold extends StatelessWidget {
  final Color backgroundColor;
  final CustomAppBar customAppBar;
  final Widget body;
  final bool autoDismissKeyboard;
  final bool resizeToAvoidBottomPadding;
  final bool resizeToAvoidBottomInset;

  const CustomScaffold({
    Key key,
    this.backgroundColor,
    this.customAppBar,
    this.body,
    this.autoDismissKeyboard = false,
    this.resizeToAvoidBottomPadding,
    this.resizeToAvoidBottomInset,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor ?? AppColors.white,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      resizeToAvoidBottomPadding: resizeToAvoidBottomPadding,
      body: Column(
        children: [
          customAppBar ?? const SizedBox(),
          Expanded(
            child: InkWell(
                onTap: autoDismissKeyboard
                    ? () {
                        CommonUtil.dismissKeyBoard(context);
                      }
                    : null,
                child: body ?? const SizedBox()),
          )
        ],
      ),
    );
  }
}
