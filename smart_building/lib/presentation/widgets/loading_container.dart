import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_building/common/bloc/loading_bloc/loading_bloc.dart';
import 'package:smart_building/common/bloc/loading_bloc/loading_state.dart';
import 'package:smart_building/common/constant/gif_constant.dart';
import 'package:smart_building/common/extensions/screen_extension.dart';

class LoadingContainer extends StatelessWidget {
  final Widget child;

  const LoadingContainer({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          child,
          BlocBuilder<LoadingBloc, LoadingState>(
            cubit: BlocProvider.of(context),
            builder: (context, state) {
              return Visibility(
                visible: state?.loading ?? true,
                child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  color: Colors.black.withOpacity(0.1),
                  child: Center(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset(
                        GifConstant.loading,
                        width: 80.w,
                        height: 80.w,
                        fit: BoxFit.scaleDown,
                      ),
                    ),
                  ),
                ),
              );
            },
          )
        ],
      ),
    );
  }

  Widget _iconClose({Function onTap}) => InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Container(
            width: 30,
            height: 30,
            padding: EdgeInsets.all(2),
            decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.8), shape: BoxShape.circle),
            child: Icon(
              Icons.close,
              color: Colors.white,
            ),
          ),
        ),
      );
}
