import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smart_building/common/constant/common_constant.dart';

class CustomCommonIcon extends StatelessWidget {
  final Color backgroundColor;
  final String icon;
  final Color colorIcon;
  final double size;

  const CustomCommonIcon(
      {Key key, this.backgroundColor, this.icon, this.colorIcon, this.size})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size ?? 48,
      height: size ?? 48,
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(size != null ? size / 3 : 16),
        boxShadow: CommonConstant.defaultShadow,
      ),
      child: Center(
        child: SvgPicture.asset(
          icon,
          width: 20,
          height: 20,
          fit: BoxFit.cover,
          color: colorIcon,
        ),
      ),
    );
  }
}
