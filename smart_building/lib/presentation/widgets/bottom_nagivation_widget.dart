import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/utils/log_util.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/presentation/journey/container/bloc/container_bloc.dart';
import 'package:smart_building/presentation/journey/container/bloc/container_state.dart';
import 'package:smart_building/presentation/journey/container/container_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

typedef OnTabChanged = void Function(int index);
const heightItem = 58.0;
class BottomNavigation extends StatefulWidget {
  final List<Widget> tabViews;
  final List<String> icons;
  final Color activeColor;
  final Color inActiveColor;
  final double iconSize;
  final OnTabChanged onTabChanged;
  final int initIndex;
  final int countItem;
  final Function iconCenterTap;

  BottomNavigation({
    Key key,
    @required this.tabViews,
    @required this.icons,
    this.initIndex = 0,
    this.activeColor = AppColors.primaryColor,
    this.inActiveColor = AppColors.grey6,
    this.iconSize = 24.0,
    this.countItem = 0,
    this.iconCenterTap,
    this.onTabChanged,
  })  : assert(tabViews != null, 'Tab view not be null'),
        assert(icons != null, 'Icons not be null'),
        super(key: key);

  @override
  State<StatefulWidget> createState() => BottomNavigationState();
}

class BottomNavigationState extends State<BottomNavigation> {
  int selectedIndex;
  List<String> _icon;

  @override
  void initState() {
    selectedIndex = widget.initIndex;
    _icon = widget.icons;
    super.initState();
  }

  void changeToTabIndex(int index) {
    if (index == 0) {
      _icon[0] = 'dashboard_selected.svg';
    } else {
      _icon[0] = 'dashboard.svg';
    }
    if (index == 1) {
      _icon[1] = 'complain_selected.svg';
    } else {
      _icon[1] = 'complain.svg';
    }
    if (index == 2) {
      _icon[2] = 'house_selected.svg';
    } else {
      _icon[2] = 'house.svg';
    }
    if (index == 3) {
      _icon[3] = 'pay_selected.svg';
    } else {
      _icon[3] = 'pay.svg';
    }
    LOG.d('changeToTabIndex: $index');
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final tabs = _icon.asMap().entries.map(
      (entry) {
        final index = entry.key;
        final source = entry.value;
        final isSelected = index == selectedIndex;
        return source != null
            ? Expanded(
                child: Material(
                  color: Colors.white,
                  child: InkWell(
                    highlightColor: AppColors.grey6,
                    splashColor: AppColors.grey6,
                    onTap: () {
                      if (!isSelected) {
                        changeToTabIndex(index);
                        if (widget.onTabChanged != null) {
                          widget.onTabChanged(selectedIndex);
                        }
                      }
                    },
                    child: Container(
                      height: heightItem,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            '${IconConstant.path}$source',
                            width: widget.iconSize,
                            height: widget.iconSize,
                            fit: BoxFit.cover,
                            color: isSelected
                                ? widget.activeColor
                                : widget.inActiveColor,
                          ),
                          Text(
                            translate(_mapIndexToString(index)),
                            style: isSelected
                                ? AppTextTheme.normalGrey.copyWith(
                                    fontWeight: FontWeight.w700,
                                    color: AppColors.primaryColorText)
                                : AppTextTheme.smallGrey,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            : const Spacer();
      },
    ).toList();
    tabs.insert(2,   SizedBox(
      width: ScreenUtil.screenWidthDp / 5,
      height:  heightItem,
    ));
    return BlocListener<ContainerBloc, ContainerState>(
      listener: (context, state) {
        if (state is ContainerMovePageState) {
          changeToTabIndex(state.pageIndex);
        }
      },
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: IndexedStack(
          index: selectedIndex,
          children: widget.tabViews,
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0,
          child: Row(
            children: tabs,
            crossAxisAlignment: CrossAxisAlignment.end,
          ),
        ),
      ),
    );
  }


  String _mapIndexToString(int index) {
    switch (index) {
      case 1:
        return ContainerConstant.complain;
      case 2:
        return ContainerConstant.home;
      case 3:
        return ContainerConstant.bill;
      default:
        return ContainerConstant.dashboard;
    }
  }
}
