import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_translate/localized_app.dart';
import 'package:smart_building/common/bloc/loading_bloc/loading_bloc.dart';
import 'package:smart_building/common/bloc/snackbar_bloc/snackbar_bloc.dart';
import 'package:smart_building/common/bloc/snackbar_bloc/snackbar_state.dart';
import 'package:smart_building/common/injector/injector.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/widgets/loading_container.dart';
import 'package:smart_building/presentation/widgets/snackbar_widget/snackbar_widget.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  List<BlocProvider> _getProviders() => [
        BlocProvider<LoadingBloc>(
            create: (_) => Injector.resolve<LoadingBloc>()),
        BlocProvider<SnackBarBloc>(
          create: (_) => Injector.resolve<SnackBarBloc>(),
        ),
      ];
  List<BlocListener> _getBlocListener(context) => [
    BlocListener<SnackBarBloc, SnackbarState>(
        listener: _mapListenerSnackbarState),
  ];

  void _mapListenerSnackbarState(BuildContext context, SnackbarState state) {
    if (state is ShowSnackBarState) {
      TopSnackBar(
        title: state.content,
        type: state.type,
      ).showWithNavigator(Routes.instance.navigatorKey.currentState, context);
    }
  }
  @override
  Widget build(BuildContext context) {
    final localizationDelegate = LocalizedApp.of(context).delegate;
    return MultiBlocProvider(
      providers: _getProviders(),
      child: MaterialApp(
        navigatorKey: Routes.instance.navigatorKey,
        debugShowCheckedModeBanner: false,
        title: 'Smart Building Application',
        onGenerateRoute: Routes.generateRoute,
        initialRoute: RouteName.loginScreen,
        theme: ThemeData(
            primaryColor: AppColors.primaryColor,
            fontFamily: 'Roboto',
            canvasColor: Colors.transparent,
            platform: TargetPlatform.iOS,
            pageTransitionsTheme: const PageTransitionsTheme(builders: {
              TargetPlatform.android: CupertinoPageTransitionsBuilder(),
              TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
            })),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          localizationDelegate
        ],
        supportedLocales: localizationDelegate.supportedLocales,
        locale: localizationDelegate.currentLocale,
        builder: (context, widget) {
          ScreenUtil.init(context);
          return LoadingContainer(
            child: MultiBlocListener(
              listeners: _getBlocListener(context),
              child: widget,
            ),
          );
        },
      ),
    );
  }
}
