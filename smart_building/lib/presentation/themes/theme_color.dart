import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xff483798);
  static const Color backgroundCommon = Color(0xffE5E5E5);
  static const Color primaryColorText = Color(0xff332C6B);
  static const Color startPurple = Color(0xff5840BB);
  static const Color endPurple = Color(0xff332C6B);
  static const Color primaryColorGradient = Color(0xff33A9FF);
  static const Color backgroundGrey = Color.fromARGB(255, 244, 245, 250);
  static const Color grey2 = Color(0xf0FAFAFA);
  static const Color grey3 = Color(0xf0f4f5fa);
  static const Color grey4 = Color(0xffE8E8E8);
  static const Color grey6 = Color(0xffBFBFBF);
  static const Color grey5 = Color(0xffD9D9D9);
  static const Color grey7 = Color(0xff8C8C8C);
  static const Color grey8 = Color.fromARGB(255, 168, 168, 178);
  static const Color grey9 = Color(0xff1d2330);
  static const Color black = Colors.black;
  static const Color blue = Color(0xff1890FF);
  static const Color white = Colors.white;
  static const Color pink = Color(0xffEA3C71);
  static const Color red1 = Color(0xffFFF1F0);
  static const Color red2 = Color(0xffFFCCC7);
  static const Color red5 = Color(0xffFF4D4F);
  static const Color red6 = Color(0xffF5222D);
  static const Color yellow = Color(0xffFFA940);
  static const Color green = Color(0xff7CE7AC);
  static const Color green1 = Color(0xffEBFAEF);
  static const Color greenText = Color(0xff7ED63E);
  static const Color lightGreen = Color(0xff8CE1A7);
  static const Color colorSun = Color(0xffFFD666);
  static const Color colorGoogle = Color(0xffdb3236);
  static const Color colorDivider = Color(0xffF2F2F2);

  // splash
  static const Color splashNotFocusDot = Color(0xffE8E8E8);

  // registration
  static const Color backgroundIconBack = Color(0xffF5F5F5);
  static const Color colorBorder = Color(0xffE8E8E8);

  // color category
  static const Color catCleanHouse = Color(0xffBC2C3D);
  static const Color catWatchHouse = Color(0xffBC2C3D);
  static const Color catGarden = Color(0xff1F356B);
  static const Color catLaundry = Color(0xff1F356B);
  static const Color catRepair = Color(0xff2D4A94);
  static const Color catPet = Color(0xffBC2C3D);
  static const Color catDelivery = Color(0xff2D4A94);
  static const Color catSurvey = Color(0xff1F356B);
  static const Color catMap = Color(0xff8E0E1D);
  static const Color catEnv = Color(0xff1F356B);
  static const Color catCamera = Color(0xff2D4A94);
  static const Color catBill = Color(0xff1F356B);
}
