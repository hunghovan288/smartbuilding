import 'package:flutter/material.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';

class AppTextTheme {
  static const smallGrey = TextStyle(
    fontSize: 12.0,
    fontWeight: FontWeight.w300,
    color: AppColors.grey7,
  );
  static const boldGrey12Sp = TextStyle(
    fontSize: 12.0,
    fontWeight: FontWeight.w700,
    color: AppColors.grey7,
  );
  static const smallBlack = TextStyle(
    fontSize: 12.0,
    fontWeight: FontWeight.w300,
    color: AppColors.black,
  );
  static const smallWhite = TextStyle(
    fontSize: 12.0,
    fontWeight: FontWeight.w300,
    color: AppColors.white,
  );
  static const normalGrey = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.grey7,
  );

  static const normalPrimary = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.primaryColorText,
  );

  static const normalBlue = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.blue,
  );
  static const normalWhite = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.white,
  );

  static const normalBlack = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.black,
  );
  static const title = TextStyle(
    fontSize: 24.0,
    fontWeight: FontWeight.w700,
    color: AppColors.grey9,
  );

  static const medium = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
    color: Colors.white,
  );
  static const medium20PxBlack = TextStyle(
    fontSize: 20.0,
    fontWeight: FontWeight.w500,
    color: AppColors.grey9,
  );
  static const mediumBlack = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
    color: AppColors.grey9,
  );
  static const bold14PxW700 = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w700,
    color: AppColors.black,
  );
  static const bold16PxW700 = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    color: AppColors.black,
  );

  static const w500Size18White = TextStyle(
    fontSize: 18.0,
    fontWeight: FontWeight.w500,
    color: AppColors.white,
  );
}
