import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/presentation/journey/auth/login/login_screen.dart';
import 'package:smart_building/presentation/journey/auth/splash/splash_screen.dart';
import 'package:smart_building/presentation/journey/container/container_screen.dart';
import 'package:smart_building/presentation/journey/feature/account/account_screen.dart';
import 'package:smart_building/presentation/journey/feature/bill/bill_screen.dart';
import 'package:smart_building/presentation/journey/feature/my_qr/my_qr_screen.dart';
import 'package:smart_building/presentation/journey/feature/notification/notification_screen.dart';
import 'package:smart_building/presentation/journey/feature/pay/pay_screen.dart';
import 'package:smart_building/presentation/journey/feature/pay_success/pay_success_screen.dart';
import 'package:smart_building/presentation/journey/feature/personal/contract/contract_screen.dart';
import 'package:smart_building/presentation/journey/feature/personal/contract_detail/contract_detail_screen.dart';
import 'package:smart_building/presentation/journey/feature/personal/juridical/juridical_screen.dart';
import 'package:smart_building/presentation/journey/feature/personal/juridical_detail/juridical_detail_screen.dart';
import 'package:smart_building/presentation/journey/feature/personal/personal_screen/personal_screen.dart';
import 'package:smart_building/presentation/journey/feature/personal/rule_detail/rule_detail_screen.dart';
import 'package:smart_building/presentation/journey/feature/personal/rules/rules_screen.dart';
import 'package:smart_building/presentation/journey/feature/service_detail/service_detail_screen.dart';
import 'package:smart_building/presentation/journey/feature/upcoming_event/upcoming_event_screen.dart';
import 'package:smart_building/presentation/z_research_widget/research_widget.dart';

class Routes {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  factory Routes() => _instance;

  Routes._internal();

  static final Routes _instance = Routes._internal();

  static Routes get instance => _instance;

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RouteName.splashScreen:
        return CupertinoPageRoute(
          builder: (context) => SplashScreen(),
        );
      case RouteName.loginScreen:
        return CupertinoPageRoute(
          builder: (context) => LoginScreen(),
        );
      case RouteName.containerScreen:
        return CupertinoPageRoute(
          builder: (context) => ScreenContainer(),
        );
      case RouteName.upcomingEventScreen:
        return CupertinoPageRoute(
          builder: (context) => UpcomingEventScreen(),
        );
      case RouteName.accountScreen:
        return CupertinoPageRoute(
          builder: (context) => AccountScreen(),
        );
      case RouteName.payScreen:
        return CupertinoPageRoute(
          builder: (context) => PayScreen(),
        );
      case RouteName.detailServiceScreen:
        return CupertinoPageRoute(
          builder: (context) => ServiceDetailScreen(
            serviceModel: settings.arguments,
          ),
        );
      case RouteName.paySuccessScreen:
        return CupertinoPageRoute(
          builder: (context) => PaySuccessScreen(),
        );
      case RouteName.myQrScreen:
        return CupertinoPageRoute(
          builder: (context) => MyQrScreen(),
        );
      case RouteName.billScreen:
        return CupertinoPageRoute(
          builder: (context) => BillScreen(),
        );
      case RouteName.personalScreen:
        return CupertinoPageRoute(
          builder: (context) => PersonalScreen(),
        );
      case RouteName.rulesScreen:
        return CupertinoPageRoute(
          builder: (context) => RulesScreen(),
        );
      case RouteName.ruleDetailScreen:
        return CupertinoPageRoute(
          builder: (context) => RuleDetailScreen(),
        );
      case RouteName.notificationScreen:
        return CupertinoPageRoute(
          builder: (context) => NotificationScreen(),
        );
      case RouteName.juridicalScreen:
        return CupertinoPageRoute(
          builder: (context) => JuridicalScreen(),
        );
      case RouteName.juridicalDetailScreen:
        return CupertinoPageRoute(
          builder: (context) => JuridicalDetailScreen(),
        );
        case RouteName.contractScreen:
        return CupertinoPageRoute(
          builder: (context) => ContractScreen(),
        );
      case RouteName.contractDetailScreen:
        return CupertinoPageRoute(
          builder: (context) => ContractDetailScreen(),
        );

      case RouteName.researchWidget:
        return CupertinoPageRoute(
          builder: (context) => ResearchWidget(),
        );
      default:
        return _emptyRoute(settings);
    }
  }

  Future<dynamic> navigateTo(String routeName, {dynamic arguments}) async {
    return navigatorKey.currentState.pushNamed(routeName, arguments: arguments);
  }

  Future<dynamic> popAndNavigateTo(
      {dynamic result, String routeName, dynamic arguments}) {
    return navigatorKey.currentState
        .popAndPushNamed(routeName, arguments: arguments);
  }

  Future<dynamic> navigateAndRemove(String routeName,
      {dynamic arguments}) async {
    return navigatorKey.currentState.pushNamedAndRemoveUntil(
      routeName,
      (Route<dynamic> route) => false,
      arguments: arguments,
    );
  }

  dynamic popUntil() {
    return navigatorKey.currentState.popUntil((route) => route.isFirst);
  }

  Future<dynamic> navigateAndReplace(String routeName,
      {dynamic arguments}) async {
    return navigatorKey.currentState
        .pushReplacementNamed(routeName, arguments: arguments);
  }

  dynamic pop({dynamic result}) {
    return navigatorKey.currentState.pop(result);
  }

  static MaterialPageRoute _emptyRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) => Scaffold(
        backgroundColor: Colors.green,
        appBar: AppBar(
          leading: InkWell(
            onTap: () => Navigator.of(context).pop(),
            child: const Center(
              child: Text(
                'Back',
                style: TextStyle(fontSize: 16),
              ),
            ),
          ),
        ),
        body: Center(
          child: Text('No path for ${settings.name}'),
        ),
      ),
    );
  }
}
