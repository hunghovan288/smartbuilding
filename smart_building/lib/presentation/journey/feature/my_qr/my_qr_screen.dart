import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/global.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/constant/image_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/presentation/journey/feature/my_qr/my_qr_constant.dart';
import 'package:smart_building/presentation/journey/feature/pay_success/pay_success_constant.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_dashed_divider.dart';
import 'package:smart_building/presentation/widgets/custom_image_network.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class MyQrScreen extends StatefulWidget {
  @override
  _PaySuccessScreenState createState() => _PaySuccessScreenState();
}

class _PaySuccessScreenState extends State<MyQrScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: Stack(
        children: [
          Image.asset(
            ImageConstant.paySuccessBackground3,
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomAppBar(
                gradient: false,
                onBack: () {
                  Routes.instance.pop();
                },
                backgroundColor: Colors.transparent,
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    translate(
                      MyQrConstant.myQrCode,
                    ),
                    style: AppTextTheme.bold16PxW700
                        .copyWith(color: AppColors.white, fontSize: 20),
                  ),
                ],
              ),
              const SizedBox(height: 33),
              Container(
                width: double.infinity,
                padding: EdgeInsets.all(16),
                margin: EdgeInsets.symmetric(
                    horizontal: PixelConstant.marginHorizontal),
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          child: CustomImageNetwork(
                            url:
                                'https://thethaiger.com/wp-content/uploads/2020/04/hoang-thuy-linh-1-1150-1586055948.jpg',
                            width: 52,
                            height: 52,
                            fit: BoxFit.cover,
                            border: 26,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(
                                width: 3, color: AppColors.primaryColor),
                            shape: BoxShape.circle,
                          ),
                        ),
                        const SizedBox(width: 16),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Thuy Linh',
                              style: AppTextTheme.bold16PxW700.copyWith(
                                  color: AppColors.primaryColorText,
                                  fontSize: 20),
                            ),
                            const SizedBox(height: 4),
                            Text(
                              translate(PaySuccessConstant.resident),
                              style: AppTextTheme.normalPrimary
                                  .copyWith(fontSize: 16),
                            ),
                          ],
                        ))
                      ],
                    ),
                    const SizedBox(height: 20),
                    CustomDashDivider(
                      color: AppColors.grey7,
                      height: 0.5,
                    ),
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40),
                      child: SvgPicture.asset(
                        IconConstant.myQrbarCode,
                        width: double.infinity,
                        height: 70,
                      ),
                    ),
                    const SizedBox(height: 40),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          IconConstant.myQrCode,
                          width: 156,
                          height: 156,
                        )
                      ],
                    ),
                    const SizedBox(height: 20),
                  ],
                ),
              ),
              const SizedBox(height: 16),
              Container(
                width: double.infinity,
                margin: EdgeInsets.symmetric(
                    horizontal: PixelConstant.marginHorizontal),
                padding: EdgeInsets.all(12),
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Row(
                  children: [
                    Container(
                      width: 48,
                      height: 48,
                      decoration: BoxDecoration(
                        color: AppColors.primaryColor,
                        shape: BoxShape.circle,
                      ),
                      child: Center(
                        child: SvgPicture.asset(
                          IconConstant.myQrLight,
                          width: 24,
                          height: 24,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    const SizedBox(width: 12),
                    Expanded(
                        child: Text(
                      translate(MyQrConstant.pleaseScan),
                      style: AppTextTheme.bold16PxW700.copyWith(
                          color: AppColors.primaryColorText, fontSize: 13),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ))
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
