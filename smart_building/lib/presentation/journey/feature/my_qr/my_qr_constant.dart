class MyQrConstant {
  static const String _path = 'MyQrConstant.';
  static const String myQrCode = '${_path}myQrCode';
  static const String myCard = '${_path}myCard';
  static const String pleaseScan = '${_path}pleaseScan';
}
