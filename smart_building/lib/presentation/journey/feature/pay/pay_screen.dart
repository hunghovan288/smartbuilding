import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/constant/string_constant.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/common/utils/common_util.dart';
import 'package:smart_building/common/utils/format_utils.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/data/models/bill_model.dart';
import 'package:smart_building/presentation/journey/feature/complain/complain_util.dart';
import 'package:smart_building/presentation/journey/feature/pay/pay_constant.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_common_button.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class PayScreen extends StatefulWidget {
  final BillModel billModel;

  const PayScreen({Key key, this.billModel}) : super(key: key);

  @override
  _PayScreenState createState() => _PayScreenState();
}

class _PayScreenState extends State<PayScreen> {
  void _onPay(){
    Routes.instance.navigateTo(RouteName.paySuccessScreen);
  }
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        onBack: () {
          Routes.instance.pop();
        },
        title: translate(StringConstant.pay),
      ),
      body: Column(
        children: [
          Container(
            width: double.infinity,
            margin: EdgeInsets.all(PixelConstant.marginHorizontal),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: CommonConstant.gradientPrimary,
                ),
                borderRadius: BorderRadius.circular(12)),
            padding: EdgeInsets.all(12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      widget.billModel?.title ?? 'Don nha theo gio',
                      style: AppTextTheme.bold16PxW700.copyWith(
                        color: AppColors.white,
                      ),
                    ),
                    const Spacer(),
                    Text(
                      'Chưa thanh toán',
                      style: AppTextTheme.normalGrey.copyWith(
                        color: AppColors.yellow,
                      ),
                    )
                  ],
                ),
                Text(
                  '#D9385347895378954',
                  style: AppTextTheme.smallGrey.copyWith(
                    color: AppColors.white,
                  ),
                ),
                const SizedBox(height: 40),
                _money(title: translate(PayConstant.price), money: 200000.0),
                const SizedBox(height: 4),
                _money(title: translate(PayConstant.surcharge), money: 0),
                const SizedBox(height: 4),
                _money(title: translate(PayConstant.totalMoney), money: 200000.0),
              ],
            ),
          ),
          const Spacer(),
          CustomCommonButton(
            text: translate(StringConstant.pay),
            padding: PixelConstant.marginHorizontal,
            onTap: _onPay,
          ),
          SizedBox(
            height: ScreenUtil.bottomBarHeight + 20,
          )
        ],
      ),
    );
  }

  Widget _money({String title, double money}) => Row(
        children: [
          Text(
            title ?? '',
            style: AppTextTheme.normalWhite.copyWith(
              fontSize: 16,
            ),
          ),
          const Spacer(),
          Text(
            FormatUtils.formatCurrencyDoubleToString(money),
            style: AppTextTheme.bold16PxW700.copyWith(
              color: AppColors.white,
            ),
          ),
        ],
      );
}
