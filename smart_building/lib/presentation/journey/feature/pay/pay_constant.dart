class PayConstant{
  static const String _path ='PayConstant.';
  static const String price ='${_path}price';
  static const String surcharge ='${_path}surcharge';
  static const String totalMoney ='${_path}totalMoney';
}