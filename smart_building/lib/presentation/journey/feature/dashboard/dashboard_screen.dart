import 'package:flutter/material.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/extensions/screen_extension.dart';
import 'package:smart_building/common/constant/image_constant.dart';
import 'package:smart_building/common/injector/injector.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/presentation/journey/container/bloc/container_bloc.dart';
import 'package:smart_building/presentation/journey/container/bloc/container_event.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/widgets/dashboard_bill_wait_pay.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/widgets/dashboard_env/dashboard_env.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/widgets/dashboard_hello_widget.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/widgets/dashboard_information.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/widgets/dashboard_recent_complain.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/widgets/dashboard_service/dashboard_service.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/widgets/dashboard_upcoming_event/dashboard_upcoming_event.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class DashBoardScreen extends StatefulWidget {
  @override
  _DashBoardScreenState createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen> {
  void _gotoAccount() {
    Routes.instance.navigateTo(RouteName.personalScreen);
  }

  void _onComplain() {
    Injector.resolve<ContainerBloc>().add(ContainerMovePageEvent(1));
  }

  void _onPay() {
    Routes.instance.navigateTo(RouteName.payScreen);
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      backgroundColor: AppColors.backgroundCommon,
      body: ListView(
        padding: EdgeInsets.only(top: 0),
        children: [
          Column(
            children: [
              Stack(
                children: [
                  Image.asset(
                    ImageConstant.backgroundTopHome,
                    height: 210.h,
                    width: double.infinity,
                    fit: BoxFit.fill,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: PixelConstant.marginHorizontal),
                    child: Column(
                      children: [
                        SizedBox(height: ScreenUtil.statusBarHeight + 20),
                        InkWell(
                          onTap: _gotoAccount,
                          child: DashBoardHelloWidget(),
                        ),
                        const SizedBox(height: 16),
                        DashBoardInformation(
                          onPay: _onPay,
                          onComplain: _onComplain,
                        ),
                        DashBoardService(),
                        DashBoardRecentComplain(),
                        DashBoardEnv(),
                        DashboardBillWaitPay(),
                        DashboardUpcomingEvent(),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
