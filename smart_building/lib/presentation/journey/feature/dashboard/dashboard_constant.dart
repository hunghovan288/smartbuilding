class DashBoardConstant{
  static const String _path = 'DashBoardConstant.';
  static const String hi = '${_path}hi';
  static const String haveNiceDay = '${_path}haveNiceDay';
  static const String billWaitingPay = '${_path}billWaitingPay';
  static const String paid = '${_path}paid';
  static const String pay = '${_path}pay';
  static const String hotLine = '${_path}hotLine';
  static const String complain = '${_path}complain';
  static const String service = '${_path}service';
  static const String serviceCleanHouse = '${_path}serviceCleanHouse';
  static const String serviceWatchHouse = '${_path}serviceWatchHouse';
  static const String serviceGarden = '${_path}serviceGarden';
  static const String serviceLaundry = '${_path}serviceLaundry';
  static const String serviceRepair = '${_path}serviceRepair';
  static const String servicePet = '${_path}servicePet';
  static const String serviceDelivery = '${_path}serviceDelivery';
  static const String serviceSurvey = '${_path}serviceSurvey';
  static const String serviceMap = '${_path}serviceMap';
  static const String serviceEnv = '${_path}serviceEnv';
  static const String serviceCamera = '${_path}serviceCamera';
  static const String serviceBill = '${_path}serviceBill';
  static const String recentComplain = '${_path}recentComplain';
  static const String upComingEvent = '${_path}upComingEvent';
  static const String temp = '${_path}temp';
  static const String humidity = '${_path}humidity';

  // pixel
  static const double heightItemWidget = 66.0;
}