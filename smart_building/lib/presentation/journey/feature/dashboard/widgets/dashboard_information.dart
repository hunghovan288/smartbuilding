import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/utils/common_util.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class DashBoardInformation extends StatelessWidget {
  final Function onPay;
  final Function onSupport;
  final Function onComplain;

  const DashBoardInformation(
      {Key key, this.onPay, this.onSupport, this.onComplain})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 12),
      decoration: BoxDecoration(
          color: AppColors.white,
          boxShadow: CommonConstant.primaryShadow,
          borderRadius: BorderRadius.circular(12)),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                translate(DashBoardConstant.billWaitingPay),
                style: AppTextTheme.smallGrey.copyWith(
                  color: AppColors.black,
                ),
              ),
              const SizedBox(height: 4),
              Row(
                children: [
                  Container(
                    width: 8,
                    height: 8,
                    decoration: BoxDecoration(
                        color: Color(0xffE45252),
                        borderRadius: BorderRadius.circular(4)),
                  ),
                  const SizedBox(width: 12),
                  Text(
                    '690.000đ',
                    style: AppTextTheme.title.copyWith(fontSize: 20),
                  )
                ],
              ),
              const SizedBox(height: 12),
              Text(
                translate(DashBoardConstant.paid),
                style: AppTextTheme.smallGrey.copyWith(
                  color: AppColors.black,
                ),
              ),
              const SizedBox(height: 4),
              Row(
                children: [
                  Container(
                    width: 8,
                    height: 8,
                    decoration: BoxDecoration(
                        color: AppColors.green,
                        borderRadius: BorderRadius.circular(4)),
                  ),
                  const SizedBox(width: 12),
                  Text(
                    '960.000đ',
                    style: AppTextTheme.title.copyWith(fontSize: 20),
                  )
                ],
              )
            ],
          )),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: onPay,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    children: [
                      Image.asset(
                        IconConstant.dashboardWallet,
                        width: 24,
                        height: 24,
                      ),
                      const SizedBox(width: 12),
                      Text(
                        translate(DashBoardConstant.pay),
                        style: AppTextTheme.bold14PxW700.copyWith(
                            foreground: CommonUtil.getVerticalGradient(colors: [
                          Color(0xff11E580),
                          Color(0xff1EE4C1)
                        ])),
                      )
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: onSupport,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    children: [
                      Image.asset(
                        IconConstant.dashboardCall,
                        width: 24,
                        height: 24,
                      ),
                      const SizedBox(width: 12),
                      Text(
                        translate(DashBoardConstant.hotLine),
                        style: AppTextTheme.bold14PxW700.copyWith(
                            foreground: CommonUtil.getVerticalGradient()),
                      )
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: onComplain,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    children: [
                      Image.asset(
                        IconConstant.dashboardComplain,
                        width: 24,
                        height: 24,
                      ),
                      const SizedBox(width: 16),
                      Text(
                        translate(DashBoardConstant.complain),
                        style: AppTextTheme.bold14PxW700.copyWith(
                            foreground: CommonUtil.getVerticalGradient(colors: [
                          Color(0xffFD0000),
                          Color(0xffFE007A)
                        ])),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ))
        ],
      ),
    );
  }
}
