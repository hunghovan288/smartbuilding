import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/constant/string_constant.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/widgets/dashboard_widget_item.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class DashboardBillWaitPay extends StatelessWidget {
  void _onItemTap() {
    Routes.instance.navigateTo(RouteName.payScreen);
  }
  void _onMore() {}
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: 8),
          Row(
            children: [
              Text(
                translate(DashBoardConstant.billWaitingPay),
                style: AppTextTheme.bold16PxW700,
              ),
              const Spacer(),
              InkWell(
                onTap: _onMore,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(16, 12, 0, 16),
                  child: Text(
                    translate(StringConstant.more),
                    style: AppTextTheme.smallGrey.copyWith(
                        color: AppColors.primaryColorText,
                        decoration: TextDecoration.underline),
                  ),
                ),
              ),
            ],
          ),
          Container(
            height: DashBoardConstant.heightItemWidget + 2,
            width: double.infinity,
            child: ListView(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.only(top: 0),
              children: MockConstant.mockBill
                  .map((e) => DashboardWidgetItem(
                        title: e.title,
                        date: e.date,
                        onTap: _onItemTap,
                      ))
                  .toList(),
            ),
          )
        ],
      ),
    );
  }
}
