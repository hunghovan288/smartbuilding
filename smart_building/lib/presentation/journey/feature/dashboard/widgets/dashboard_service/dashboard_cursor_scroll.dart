import 'package:flutter/material.dart';
import 'package:smart_building/common/utils/log_util.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';

class DashBoardCursorScroll extends StatelessWidget {
  final double alpha;

  const DashBoardCursorScroll({Key key, this.alpha}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      alignment: Alignment.center,
      child: Stack(
        children: [
          Container(
            width: 40,
            height: 12,
            decoration: BoxDecoration(
                color: AppColors.grey3, borderRadius: BorderRadius.circular(6)),
          ),
          Positioned(
            left: (alpha * 20),
            child: Container(
              width: 20,
              height: 12,
              decoration: BoxDecoration(
                  color: AppColors.primaryColor,
                  borderRadius: BorderRadius.circular(6)),
            ),
          ),
        ],
      ),
    );
  }
}
