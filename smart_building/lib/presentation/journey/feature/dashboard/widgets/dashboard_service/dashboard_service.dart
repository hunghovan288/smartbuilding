import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/common/utils/log_util.dart';
import 'package:smart_building/data/models/service_model.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/widgets/dashboard_service/dashboard_cursor_scroll.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/widgets/dashboard_service/dashboard_service_item.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class DashBoardService extends StatefulWidget {
  @override
  _DashBoardServiceState createState() => _DashBoardServiceState();
}

class _DashBoardServiceState extends State<DashBoardService> {
  ScrollController _controller;

  List<List<ServiceModel>> _dataServiceModels = [];

  @override
  void initState() {
    _controller = ScrollController()..addListener(_scrollListener);
    _controller.addListener(_scrollListener);
    for (int i = 0; i < MockConstant.mockServices().length; i += 3) {
      if (i + 3 >= MockConstant.mockServices().length) {
        _dataServiceModels.add(MockConstant.mockServices()
            .sublist(i, MockConstant.mockServices().length));
        break;
      }
      _dataServiceModels.add(MockConstant.mockServices().sublist(i, i + 3));
    }
    super.initState();
  }

  void _scrollListener() {
    // setState(() {});
  }

  void _onItemTap(ServiceModel serviceModel) {
    Routes.instance.navigateTo(RouteName.detailServiceScreen,arguments: serviceModel);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 16),
          Text(
            translate(DashBoardConstant.service),
            style: AppTextTheme.bold16PxW700,
          ),
          const SizedBox(height: 12),
          Container(
            width: double.infinity,
            height: 280,
            child: ListView(
              padding: EdgeInsets.only(top: 0),
              controller: _controller,
              scrollDirection: Axis.horizontal,
              children: _dataServiceModels
                  .map((e) => DashBoardServiceThreeItem(
                        serviceModels: e,
                        onTap: _onItemTap,
                      ))
                  .toList(),
            ),
          ),
          // DashBoardCursorScroll(
          //   alpha: _controller.hasClients
          //       ? (_controller?.position?.pixels ?? 0) /
          //           (_controller?.position?.maxScrollExtent ?? 1)
          //       : 0,
          // ),
        ],
      ),
    );
  }
}
