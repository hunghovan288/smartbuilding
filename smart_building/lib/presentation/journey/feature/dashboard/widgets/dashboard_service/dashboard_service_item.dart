import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/data/models/service_model.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

// map map từng 3 item một
class DashBoardServiceThreeItem extends StatelessWidget {
  final List<ServiceModel> serviceModels;
  final Function(ServiceModel serviceModel) onTap;

  const DashBoardServiceThreeItem({Key key, this.serviceModels, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: serviceModels
          .map((e) => Padding(
                padding: const EdgeInsets.only(bottom: 16),
                child: DashBoardServiceItem(
                  serviceModel: e,
                  onTap: onTap,
                ),
              ))
          .toList(),
    );
  }
}

class DashBoardServiceItem extends StatelessWidget {
  final ServiceModel serviceModel;
  final Function(ServiceModel serviceModel) onTap;

  const DashBoardServiceItem({
    Key key,
    this.serviceModel,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        onTap(serviceModel);
      },
      child: Container(
        width: 90,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 48,
              height: 48,
              decoration: BoxDecoration(
                color: serviceModel.color,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Center(
                child: SvgPicture.asset(
                  serviceModel.icon,
                  width: 24,
                  height: 24,
                ),
              ),
            ),
            const SizedBox(height: 10),
            Text(
              translate(serviceModel.text) ?? '',
              style: AppTextTheme.smallGrey.copyWith(
                fontSize: 14,
                color: AppColors.black,
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }
}
