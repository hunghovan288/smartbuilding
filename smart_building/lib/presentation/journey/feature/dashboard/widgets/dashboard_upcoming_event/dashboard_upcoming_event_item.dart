import 'package:flutter/material.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/data/models/event_model.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/custom_image_network.dart';

class DashBoardUpcomingEventItem extends StatelessWidget {
  final EventModel eventModel;
  final Function(EventModel eventModel) onTap;

  const DashBoardUpcomingEventItem({
    Key key,
    this.eventModel,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        onTap(eventModel);
      },
      child: Container(
        height: 120,
        width: ScreenUtil.screenWidthDp * 2 / 3,
        margin: EdgeInsets.only(right: PixelConstant.marginHorizontal),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
        ),
        child: Stack(
          children: [
            CustomImageNetwork(
              url: eventModel.icon,
              height: double.infinity,
              width: double.infinity,
              fit: BoxFit.cover,
              border: 12,
            ),
            Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                color: AppColors.black.withOpacity(0.4),
                borderRadius: BorderRadius.circular(12),
              ),
            ),
            Column(
              children: [
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Text(
                    eventModel.name,
                    style: AppTextTheme.bold14PxW700.copyWith(
                      color: AppColors.white,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
