import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/constant/string_constant.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/widgets/dashboard_upcoming_event/dashboard_upcoming_event_item.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class DashboardUpcomingEvent extends StatelessWidget {
  void _onMore() {
    Routes.instance.navigateTo(RouteName.upcomingEventScreen);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(top: 8, bottom: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                translate(DashBoardConstant.upComingEvent),
                style: AppTextTheme.bold16PxW700,
              ),
              const Spacer(),
              InkWell(
                onTap: _onMore,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(16, 12, 0, 16),
                  child: Text(
                    translate(StringConstant.more),
                    style: AppTextTheme.smallGrey.copyWith(
                        color: AppColors.primaryColorText,
                        decoration: TextDecoration.underline),
                  ),
                ),
              ),
            ],
          ),
          Container(
            width: double.infinity,
            height: 120,
            child: ListView(
              padding: EdgeInsets.only(top: 0),
              scrollDirection: Axis.horizontal,
              children: MockConstant.mockEvent
                  .map((e) => DashBoardUpcomingEventItem(
                        eventModel: e,
                        onTap: (eventModel) {
                          _onMore();
                        },
                      ))
                  .toList(),
            ),
          )
        ],
      ),
    );
  }
}
