import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/custom_image_network.dart';

class DashBoardHelloWidget extends StatelessWidget {
  void _gotoNotificationScreen() {
    Routes.instance.navigateTo(RouteName.notificationScreen);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CustomImageNetwork(
          url: MockConstant.urlAvatar,
          width: 52,
          height: 52,
          fit: BoxFit.cover,
          border: 26,
        ),
        const SizedBox(width: 10),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  '${translate(DashBoardConstant.hi)}, Hoang Linh!',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: AppTextTheme.title.copyWith(
                    fontSize: 22,
                    color: AppColors.white,
                  ),
                ),
                const Spacer(),
                InkWell(
                  onTap: _gotoNotificationScreen,
                  child: Container(
                      width: 40,
                      height: 40,
                      child: Center(
                          child: SvgPicture.asset(
                        IconConstant.dashboardBell,
                        width: 20,
                        height: 20,
                      ))),
                )
              ],
            ),
            Text(
              translate(DashBoardConstant.haveNiceDay),
              style: AppTextTheme.smallGrey.copyWith(
                color: AppColors.white,
              ),
            )
          ],
        )),
      ],
    );
  }
}
