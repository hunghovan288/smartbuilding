import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class DashBoardEnv extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(top: 20),
      decoration: BoxDecoration(
          color: AppColors.white,
          boxShadow: CommonConstant.primaryShadow,
          borderRadius: BorderRadius.circular(12)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: [
                Image.asset(
                  IconConstant.dashboardCurrentLocation,
                  width: 24,
                  height: 24,
                ),
                const SizedBox(width: 4),
                Text(
                  'Mỹ Đình, Hà Nội',
                  style: AppTextTheme.smallBlack,
                )
              ],
            ),
          ),
          Divider(
            height: 2,
          ),
          Container(
            padding: const EdgeInsets.all(12.0),
            width: double.infinity,
            height: 60,
            child: Row(
              children: [
                Expanded(
                    child: _itemEnv(
                  title: translate(DashBoardConstant.temp),
                  content: '25 C',
                )),
                const VerticalDivider(width: 2),
                Expanded(
                    child: _itemEnv(
                  title: translate(DashBoardConstant.humidity),
                  content: '94.6%',
                )),
                const VerticalDivider(width: 2),
                Expanded(
                    child: _itemEnv(
                  title: 'PM2.5',
                  content: '13.9 ug/m3',
                )),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(12, 0, 12, 12),
            height: 55,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: AppColors.primaryColor),
            child: Stack(
              children: [
                Container(
                  width: double.infinity,
                  height: double.infinity,
                  color: AppColors.white.withOpacity(0.3),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Image.asset(
                        IconConstant.dashboardFace,
                        width: 24,
                        height: 24,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            '25',
                            style: AppTextTheme.bold14PxW700.copyWith(
                              color: AppColors.white,
                              fontSize: 20,
                            ),
                          ),
                          Text(
                            'VN AQI',
                            style: AppTextTheme.bold14PxW700.copyWith(
                              color: AppColors.white,
                              fontSize: 10,
                            ),
                          )
                        ],
                      ),
                      Text(
                        'Tot',
                        style: AppTextTheme.bold14PxW700.copyWith(
                          color: AppColors.white,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _itemEnv({String title, String content}) => Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            title ?? '',
            style: AppTextTheme.smallBlack.copyWith(fontSize: 10),
          ),
          const SizedBox(height: 6),
          Text(
            content ?? '',
            style: AppTextTheme.bold14PxW700.copyWith(fontSize: 12),
          )
        ],
      );
}
