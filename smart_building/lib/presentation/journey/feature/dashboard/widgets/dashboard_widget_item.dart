import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class DashboardWidgetItem extends StatelessWidget {
  final String title;
  final String date;
  final Function onTap;

  const DashboardWidgetItem({Key key, this.title, this.date, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: ScreenUtil.screenWidthDp * 2 / 3,
        padding: EdgeInsets.all(12),
        margin: EdgeInsets.only(right: 16),
        height: DashBoardConstant.heightItemWidget,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: AppColors.primaryColor,
          gradient: LinearGradient(
            colors: CommonConstant.gradientPrimary,
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  title ?? '',
                  style:
                      AppTextTheme.bold16PxW700.copyWith(color: AppColors.white),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(height: 6),
                Text(
                  date ?? '',
                  style: AppTextTheme.normalWhite.copyWith(fontSize: 12),
                )
              ],
            )),
            SvgPicture.asset(
              IconConstant.right,
              width: 16,
              height: 16,
            )
          ],
        ),
      ),
    );
  }
}
