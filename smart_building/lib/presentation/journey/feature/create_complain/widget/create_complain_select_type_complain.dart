import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/presentation/journey/feature/create_complain/create_complain_constant.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/bottom_sheet_container.dart';

const _typeComplain = [
  'Dịch vụ',
  'Bảo vệ',
  'Môi trường',
  'Item 1',
  'Item 2',
];

class CreateComplainSelectTypeComplain extends StatelessWidget {
  final String typeSelected;
  final Function(String text) onItemTap;

  const CreateComplainSelectTypeComplain({
    Key key,
    this.typeSelected,
    this.onItemTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomSheetContainer(
      title: translate(CreateComplainConstant.typeComplain),
      onClose: (){
        Routes.instance.pop();
      },
      child: ListView(
        children: _typeComplain
            .map((e) => _item(e, onTap: onItemTap, context: context))
            .toList(),
      ),
    );
  }

  Widget _item(String type,
          {Function(String type) onTap, BuildContext context}) =>
      InkWell(
        onTap: () {
          Navigator.pop(context);
          onTap(type);
        },
        child: SizedBox(
          height: 56,
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Spacer(),
              Row(
                children: [
                  SizedBox(width: 16.0),
                  Expanded(
                    child: Text(
                      type ?? '',
                      textAlign: TextAlign.start,
                      style: AppTextTheme.normalBlack,
                    ),
                  ),
                  type == (typeSelected??'')
                      ? Icon(
                          Icons.check,
                          color: AppColors.primaryColor,
                        )
                      : SizedBox(),
                  SizedBox(width: 16.0),
                ],
              ),
              const Spacer(),
              Divider(
                height: 1.0,
                color: Colors.grey[300],
              )
            ],
          ),
        ),
      );
}
