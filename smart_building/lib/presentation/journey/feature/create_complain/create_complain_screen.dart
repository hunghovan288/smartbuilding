import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/bloc/snackbar_bloc/snackbar_bloc.dart';
import 'package:smart_building/common/bloc/snackbar_bloc/snackbar_event.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/constant/string_constant.dart';
import 'package:smart_building/common/injector/injector.dart';
import 'package:smart_building/common/utils/common_util.dart';
import 'package:smart_building/common/utils/validate_utils.dart';
import 'package:smart_building/presentation/journey/feature/create_complain/create_complain_constant.dart';
import 'package:smart_building/presentation/journey/feature/create_complain/create_complain_utils.dart';
import 'package:smart_building/presentation/journey/feature/create_complain/widget/create_complain_select_type_complain.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/bottom_sheet_container.dart';
import 'package:smart_building/presentation/widgets/custom_common_button.dart';
import 'package:smart_building/presentation/widgets/custom_textfield_border.dart';
import 'package:smart_building/presentation/widgets/snackbar_widget/enum.dart';

class CreateComplainBottomSheet extends StatefulWidget {
  @override
  _CreateComplainBottomSheetState createState() =>
      _CreateComplainBottomSheetState();
}

class _CreateComplainBottomSheetState extends State<CreateComplainBottomSheet> {
  String _typeComplainSelected;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _titleController;
  TextEditingController _contentController;

  @override
  void initState() {
    _titleController = TextEditingController()
      ..addListener(_listenTextController);
    _contentController = TextEditingController()
      ..addListener(_listenTextController);
    super.initState();
  }

  void _listenTextController() {
    setState(() {});
  }

  @override
  void dispose() {
    _titleController?.dispose();
    _contentController?.dispose();
    super.dispose();
  }

  void _onSelectTypeComplain() {
    CommonUtil.showCustomBottomSheet(
        context: context,
        height: 350,
        child: CreateComplainSelectTypeComplain(
          typeSelected: _typeComplainSelected,
          onItemTap: (text) {
            setState(() {
              _typeComplainSelected = text;
            });
          },
        ));
  }

  void _onDone() async {
    if (!CommonUtil.validateAndSave(_formKey)) return;
    Injector.resolve<SnackBarBloc>().add(ShowSnackbarEvent(
        type: SnackBarType.success, content: 'Tạo đơn khiếu nại thành công!'));
    await Future.delayed(Duration(milliseconds: 1000));
    Navigator.pop(context);
    Routes.instance.pop();
  }

  @override
  Widget build(BuildContext context) {
    return BottomSheetContainer(
      title: translate(CreateComplainConstant.createComplain),
      onClose: () {
        Routes.instance.pop();
      },
      child: InkWell(
        onTap: () {
          CommonUtil.dismissKeyBoard(context);
        },
        child: Padding(
          padding: const EdgeInsets.all(PixelConstant.marginHorizontal),
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    translate(CreateComplainConstant.typeComplain),
                    style: AppTextTheme.normalBlack,
                  ),
                  const Spacer(),
                  InkWell(
                    onTap: _onSelectTypeComplain,
                    child: Row(
                      children: [
                        Text(
                          _typeComplainSelected ??
                              translate(CreateComplainConstant.select),
                          style: AppTextTheme.normalGrey,
                        ),
                        const SizedBox(width: 8),
                        Icon(
                          Icons.keyboard_arrow_down_outlined,
                          color: AppColors.grey7,
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(height: 20.0),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _titleController.text.isNotEmpty
                          ? translate(StringConstant.title)
                          : '',
                      style: AppTextTheme.normalGrey,
                    ),
                    CustomTextFieldBorder(
                      hintText: translate(StringConstant.title),
                      textInputAction: TextInputAction.done,
                      controller: _titleController,
                      contentPaddingHorizontal: 10.0,
                      contentPaddingVertical: 10.0,
                      onValidate: (text) {
                        return CreateComplainUtils.validTitleCreateComplain(
                            text,
                            translate(CreateComplainConstant.titleWarning));
                      },
                    ),
                    SizedBox(height: 20.0),
                    Text(
                      _contentController.text.isNotEmpty
                          ? translate(StringConstant.content)
                          : '',
                      style: AppTextTheme.normalGrey,
                    ),
                    CustomTextFieldBorder(
                        hintText: translate(StringConstant.content),
                        maxLine: null,
                        contentPaddingHorizontal: 10.0,
                        controller: _contentController,
                        textInputAction: TextInputAction.newline,
                        contentPaddingVertical: 10.0,
                        onValidate: (text) {
                          return CreateComplainUtils
                              .validTiContentCreateComplain(
                                  text,
                                  translate(
                                      CreateComplainConstant.contentWarning));
                        }),
                  ],
                ),
              ),
              const Spacer(),
              CustomCommonButton(
                text: translate(CreateComplainConstant.sendComplain),
                onTap: _onDone,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
