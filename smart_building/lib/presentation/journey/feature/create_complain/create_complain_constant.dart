class CreateComplainConstant {
  static const String _path = 'CreateComplainConstant.';
  static const String createComplain = '${_path}createComplain';
  static const String typeComplain = '${_path}typeComplain';
  static const String select = '${_path}select';
  static const String contentWarning = '${_path}contentWarning';
  static const String titleWarning = '${_path}titleWarning';
  static const String sendComplain = '${_path}sendComplain';

}
