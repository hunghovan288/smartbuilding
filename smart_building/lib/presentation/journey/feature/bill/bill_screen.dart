import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/constant/string_constant.dart';
import 'package:smart_building/data/models/service_model.dart';
import 'package:smart_building/presentation/journey/feature/bill/widgets/bill_widget_item.dart';
import 'package:smart_building/presentation/journey/feature/service_detail/widgets/service_detail_widget_item.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class BillScreen extends StatefulWidget {


  @override
  _BillScreenState createState() => _BillScreenState();
}

class _BillScreenState extends State<BillScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        title: translate(StringConstant.bill) ?? '',
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(
          horizontal: PixelConstant.marginHorizontal,
          vertical: 16,
        ),
        children: [
          BillWidgetItem(),
          BillWidgetItem(),
          BillWidgetItem(),
          BillWidgetItem(),
          BillWidgetItem(),
          BillWidgetItem(),
          BillWidgetItem(),
        ],
      ),
    );
  }
}
