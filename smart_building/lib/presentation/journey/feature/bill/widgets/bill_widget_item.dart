import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class BillWidgetItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Routes.instance.navigateTo(RouteName.payScreen);
      },
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.only(bottom: 12),
        padding: const EdgeInsets.all(12.0),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: CommonConstant.gradientPrimary,
          ),
          borderRadius: BorderRadius.circular(12),
        ),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Text(
                    'Dich vu don nha',
                    style: AppTextTheme.w500Size18White,
                  ),
                ),
                SvgPicture.asset(
                  IconConstant.serviceDetailLock,
                  width: 20,
                  height: 20,
                ),
                const SizedBox(width: 4),
                Text(
                  '200.000đ',
                  style: AppTextTheme.normalWhite.copyWith(fontSize: 12),
                )
              ],
            ),
            const SizedBox(height: 16),
            Row(
              children: [
                Text(
                  '10/10/2020',
                  style: AppTextTheme.normalWhite.copyWith(fontSize: 12),
                ),
                const Spacer(),
                Text(
                  'Đã thanh toán',
                  style: AppTextTheme.normalWhite
                      .copyWith(fontSize: 12, color: AppColors.greenText),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
