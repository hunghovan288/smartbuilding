import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/presentation/journey/feature/notification/notification_constant.dart';
import 'package:smart_building/presentation/journey/feature/notification/widgets/notification_item.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  void _onBack() {
    Routes.instance.pop();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      autoDismissKeyboard: false,
      customAppBar: CustomAppBar(
        title: translate(NotificationConstant.notification),
        onBack: _onBack,
      ),
      body: ListView(
        padding: EdgeInsets.all(PixelConstant.marginHorizontal),
        children: [
          Text(
            translate(NotificationConstant.today),
            style: AppTextTheme.smallGrey,
          ),
          const SizedBox(height: 4),
          NotificationItem(
            notiModel: MockConstant.mockNotification()[0],
          ),
          NotificationItem(
            notiModel: MockConstant.mockNotification()[1],
          ),
          const SizedBox(height: 12),
          Text(
            translate(NotificationConstant.past),
            style: AppTextTheme.smallGrey,
          ),
          const SizedBox(height: 4),
          NotificationItem(
            notiModel: MockConstant.mockNotification()[2],
          ),
          NotificationItem(
            notiModel: MockConstant.mockNotification()[3],
          ),
        ],
      ),
    );
  }
}
