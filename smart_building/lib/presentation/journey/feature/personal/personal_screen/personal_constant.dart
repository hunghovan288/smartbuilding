class PersonalConstant{
  static const String _path ='PersonalConstant.';
  static const String account ='${_path}account';
  static const String contract ='${_path}contract';
  static const String juridical ='${_path}juridical';
  static const String rules ='${_path}rules';
  static const String logout ='${_path}logout';
}