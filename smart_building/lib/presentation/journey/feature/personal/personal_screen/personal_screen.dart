import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/constant/image_constant.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/extensions/screen_extension.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/presentation/journey/feature/personal/personal_screen/personal_constant.dart';
import 'package:smart_building/presentation/journey/feature/personal/personal_screen/widgets/personal_option.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/custom_common_button.dart';
import 'package:smart_building/presentation/widgets/custom_image_network.dart';

class PersonalScreen extends StatefulWidget {
  @override
  _PersonalScreneState createState() => _PersonalScreneState();
}

class _PersonalScreneState extends State<PersonalScreen> {
  void _onBack(){
    Routes.instance.pop();
  }
  void _onLogOut(){
    Routes.instance.navigateAndRemove(RouteName.loginScreen);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundCommon,
      body: Stack(
        children: [
          Image.asset(
            ImageConstant.backgroundTopHome,
            height: 220.h,
            width: double.infinity,
            fit: BoxFit.fill,
          ),
          SizedBox(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: ScreenUtil.statusBarHeight + 30,
                ),
                CustomImageNetwork(
                  url: MockConstant.urlAvatar,
                  width: 75.h,
                  height: 75.h,
                  fit: BoxFit.cover,
                  border: 37.h,
                ),
                const SizedBox(height: 12),
                Text(
                  'Hoàng Thùy Linh',
                  style: AppTextTheme.w500Size18White,
                ),
                Text(
                  'CT3-PC1305',
                  style: AppTextTheme.smallWhite.copyWith(fontSize: 14),
                ),
                const SizedBox(height: 50),
                PersonalOption(),
                const Spacer(),
                CustomCommonButton(
                  text: translate(PersonalConstant.logout),
                  padding: PixelConstant.marginHorizontal,
                  onTap: _onLogOut,
                ),
                SizedBox(height: 16 + ScreenUtil.bottomBarHeight),
              ],
            ),
          ),
          Positioned(
            child: InkWell(
              onTap: _onBack,
              child: Padding(
                padding: const EdgeInsets.all(PixelConstant.marginHorizontal),
                child: SvgPicture.asset(
                  IconConstant.back,
                  width: 16,
                  height: 16,
                  fit: BoxFit.cover,
                  color: AppColors.white,
                ),
              ),
            ),
            top: ScreenUtil.statusBarHeight + 12,
            left: 0,
          ),
          Positioned(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: SvgPicture.asset(
                IconConstant.personalSetting,
                width: 22,
                height: 22,
                fit: BoxFit.cover,
                color: AppColors.white,
              ),
            ),
            top: ScreenUtil.statusBarHeight + 12,
            right: 0,
          )
        ],
      ),
    );
  }
}
