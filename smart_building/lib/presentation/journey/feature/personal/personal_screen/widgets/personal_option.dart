import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/presentation/journey/feature/personal/personal_screen/personal_constant.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class PersonalOption extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 12),
      margin: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
      decoration: BoxDecoration(
          color: AppColors.white, borderRadius: BorderRadius.circular(12)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          PersonalOptionItem(
            onTap: () {
              Routes.instance.navigateTo(RouteName.accountScreen);
            },
            title: translate(PersonalConstant.account),
            icon: IconConstant.personalAccount,
          ),
          PersonalOptionItem(
            onTap: () {
              Routes.instance.navigateTo(RouteName.rulesScreen);
            },
            title: translate(PersonalConstant.rules),
            icon: IconConstant.personalRules,
          ),
          PersonalOptionItem(
            onTap: () {
              Routes.instance.navigateTo(RouteName.contractScreen);
            },
            title: translate(PersonalConstant.contract),
            icon: IconConstant.personalContract,
          ),
          PersonalOptionItem(
            onTap: () {
              Routes.instance.navigateTo(RouteName.juridicalScreen);
            },
            title: translate(PersonalConstant.juridical),
            icon: IconConstant.personalJuridical,
          ),
        ],
      ),
    );
  }
}

class PersonalOptionItem extends StatelessWidget {
  final String title;
  final String icon;
  final Function onTap;

  const PersonalOptionItem({Key key, this.title, this.icon, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        height: 48,
        child: Column(
          children: [
            Expanded(
                child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  icon,
                  width: 20,
                  height: 20,
                ),
                const SizedBox(width: 8),
                Text(
                  title ?? '',
                  style: AppTextTheme.normalPrimary.copyWith(fontSize: 16),
                ),
                const Spacer(),
                SvgPicture.asset(
                  IconConstant.right,
                  width: 18,
                  height: 18,
                  color: AppColors.primaryColor,
                ),
                const SizedBox(width: 8),
              ],
            )),
            Divider(
              color: AppColors.grey5,
              height: 1,
            )
          ],
        ),
      ),
    );
  }
}
