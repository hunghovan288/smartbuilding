import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/presentation/journey/feature/personal/personal_screen/personal_constant.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class RuleDetailScreen extends StatefulWidget {
  @override
  _RulesScreenState createState() => _RulesScreenState();
}

class _RulesScreenState extends State<RuleDetailScreen> {
  void _onBack() {
    Routes.instance.pop();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      backgroundColor: AppColors.backgroundCommon,
      customAppBar: CustomAppBar(
        title: translate(PersonalConstant.rules),
        onBack: _onBack,
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: Container(
              width: double.infinity,
              margin: EdgeInsets.all(PixelConstant.marginHorizontal),
              decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(12)),
              child: ListView(
                padding: EdgeInsets.all(16),
                children: [
                  const SizedBox(height: 4),
                  Text(
                    'NỘI QUY CHUNG CƯ',
                    textAlign: TextAlign.center,
                    style: AppTextTheme.w500Size18White
                        .copyWith(color: Color(0xff1658EB)),
                    maxLines: 2,
                  ),
                  const SizedBox(height: 16),
                  Text(
                    MockConstant.mockDetailRule,
                    style: AppTextTheme.normalGrey.copyWith(fontSize: 16),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
