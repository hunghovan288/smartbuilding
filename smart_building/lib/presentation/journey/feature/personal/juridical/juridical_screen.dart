import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/presentation/journey/feature/personal/juridical/widgets/juridical_item_widget.dart';
import 'package:smart_building/presentation/journey/feature/personal/personal_screen/personal_constant.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class JuridicalScreen extends StatefulWidget {
  @override
  _JuridicalScreenState createState() => _JuridicalScreenState();
}

class _JuridicalScreenState extends State<JuridicalScreen> {
  void _onBack() {
    Routes.instance.pop();
  }

  void _gotoJuridicalDetail() {
    Routes.instance.navigateTo(RouteName.juridicalDetailScreen);
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      backgroundColor: AppColors.backgroundCommon,
      customAppBar: CustomAppBar(
        onBack: _onBack,
        title: translate(PersonalConstant.juridical),
      ),
      body: ListView(
        padding: EdgeInsets.all(16),
        children: [
          JuridicalItemWidget(
            title: 'Giấy phép xây dựng',
            onTap: _gotoJuridicalDetail,
          ),
          const SizedBox(height: 12),
          JuridicalItemWidget(
            title: 'Quyền sử dụng đất',
            onTap: _gotoJuridicalDetail,
          ),
          const SizedBox(height: 12),
          JuridicalItemWidget(
            title: 'Thiết kế và bản vẽ công trình',
            onTap: _gotoJuridicalDetail,
          ),
          const SizedBox(height: 12),
          JuridicalItemWidget(
            title: 'Quyết định đầu tư dự án',
            onTap: _gotoJuridicalDetail,
          ),
        ],
      ),
    );
  }
}
