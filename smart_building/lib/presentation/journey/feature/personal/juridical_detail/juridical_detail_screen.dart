import 'package:flutter/material.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class JuridicalDetailScreen extends StatefulWidget {
  @override
  _JuridicalDetailScreenState createState() => _JuridicalDetailScreenState();
}

class _JuridicalDetailScreenState extends State<JuridicalDetailScreen> {
  void _onBack() {
    Routes.instance.pop();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        backgroundColor: AppColors.backgroundCommon,
        onBack: _onBack,
        title: 'Giấy phép xây dựng',
      ),
      body: Padding(
        padding: const EdgeInsets.all(PixelConstant.marginHorizontal),
        child: Image.asset(
          MockConstant.detailJuridical,
          width: double.infinity,
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }
}
