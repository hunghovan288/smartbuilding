import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/presentation/journey/feature/personal/personal_screen/personal_constant.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class ContractDetailScreen extends StatefulWidget {
  @override
  _ContractDetailScreenState createState() => _ContractDetailScreenState();
}

class _ContractDetailScreenState extends State<ContractDetailScreen> {
  void _onBack() {
    Routes.instance.pop();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        backgroundColor: AppColors.backgroundCommon,
        onBack: _onBack,
        title: translate(PersonalConstant.contract),
      ),
      body: Padding(
        padding: const EdgeInsets.all(PixelConstant.marginHorizontal),
        child: Image.asset(
          MockConstant.detailJuridical,
          width: double.infinity,
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }
}
