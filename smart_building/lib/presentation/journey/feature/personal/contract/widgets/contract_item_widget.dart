import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class ContractItemWidget extends StatelessWidget {
  final String title;
  final Function onTap;
  const ContractItemWidget({Key key, this.title, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        height: 74,
        padding: EdgeInsets.symmetric(horizontal: 12),
        decoration: BoxDecoration(
            color: AppColors.white, borderRadius: BorderRadius.circular(12)),
        child: Row(
          children: [
            SvgPicture.asset(
              MockConstant.pdf,
              width: 42,
              height: 50,
            ),
            const SizedBox(width: 16),
            Text(
              title ?? '',
              style: AppTextTheme.bold16PxW700,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }
}
