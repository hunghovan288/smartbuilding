import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/presentation/journey/feature/personal/contract/widgets/contract_item_widget.dart';
import 'package:smart_building/presentation/journey/feature/personal/juridical/widgets/juridical_item_widget.dart';
import 'package:smart_building/presentation/journey/feature/personal/personal_screen/personal_constant.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class ContractScreen extends StatefulWidget {
  @override
  _ContractScreenState createState() => _ContractScreenState();
}

class _ContractScreenState extends State<ContractScreen> {
  void _onBack() {
    Routes.instance.pop();
  }

  void _gotoContractDetail() {
    Routes.instance.navigateTo(RouteName.contractDetailScreen);
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      backgroundColor: AppColors.backgroundCommon,
      customAppBar: CustomAppBar(
        onBack: _onBack,
        title: translate(PersonalConstant.contract),
      ),
      body: ListView(
        padding: EdgeInsets.all(16),
        children: [
          ContractItemWidget(
            title: 'Hợp đồng xây dựng',
            onTap: _gotoContractDetail,
          ),
          const SizedBox(height: 12),
          ContractItemWidget(
            title: 'Hợp đồng thuê căn hộ',
            onTap: _gotoContractDetail,
          ),
          const SizedBox(height: 12),
          ContractItemWidget(
            title: 'Hợp đồng thuê văn phòng',
            onTap: _gotoContractDetail,
          ),
        ],
      ),
    );
  }
}
