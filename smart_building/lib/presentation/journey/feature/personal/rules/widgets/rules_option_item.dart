import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class RulesOptionItem extends StatelessWidget {
  final String title;

  final Function onTap;

  const RulesOptionItem({Key key, this.title,   this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        height: 48,
        child: Column(
          children: [
            Expanded(
                child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(width: 8),
                Text(
                  title ?? '',
                  style: AppTextTheme.normalPrimary.copyWith(fontSize: 16),
                ),
                const Spacer(),
                SvgPicture.asset(
                  IconConstant.right,
                  width: 18,
                  height: 18,
                  color: AppColors.primaryColor,
                ),
                const SizedBox(width: 8),
              ],
            )),
            Divider(
              color: AppColors.grey5,
              height: 1,
            )
          ],
        ),
      ),
    );
  }
}
