import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/presentation/journey/feature/personal/personal_screen/personal_constant.dart';
import 'package:smart_building/presentation/journey/feature/personal/rules/widgets/rules_option_item.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class RulesScreen extends StatefulWidget {
  @override
  _RulesScreenState createState() => _RulesScreenState();
}

class _RulesScreenState extends State<RulesScreen> {
  void _onBack(){
    Routes.instance.pop();
  }
  void _gotoRulesDetail(){
    Routes.instance.navigateTo(RouteName.ruleDetailScreen);
  }
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      backgroundColor: AppColors.backgroundCommon,
      customAppBar: CustomAppBar(
        title: translate(PersonalConstant.rules),
        onBack: _onBack,
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(horizontal: 12),
            margin: EdgeInsets.all(PixelConstant.marginHorizontal),
            decoration: BoxDecoration(
                color: AppColors.white, borderRadius: BorderRadius.circular(12)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RulesOptionItem(
                  title: 'Nội quy cư dân nhà tầng thấp',
                  onTap: _gotoRulesDetail,
                ),
                RulesOptionItem(
                  title: 'Nội quy cư dân nhà tầng cao',
                  onTap: _gotoRulesDetail,
                ),
                RulesOptionItem(
                  title: 'Nội quy cảnh quan',
                  onTap: _gotoRulesDetail,
                ),
                RulesOptionItem(
                  title: 'Nội quy căn hộ',
                  onTap: _gotoRulesDetail,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
