class AccountConstant{
  static const String _path ='AccountConstant.';
  static const String account ='${_path}account';
  static const String cardManager ='${_path}cardManager';
  static const String completeInfo ='${_path}completeInfo';
  static const String yourInfo ='${_path}yourInfo';
  static const String provideEnoughInfo ='${_path}provideEnoughInfo';
  static const String verifyEmail ='${_path}verifyEmail';
  static const String logout ='${_path}logout';

  static const double heightItemCard = 160;
}