import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/data/models/card_model.dart';
import 'package:smart_building/presentation/journey/feature/account/account_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class AccountCard extends StatelessWidget {
  final CardModel cardModel;
  final Function(CardModel cardModel) onChangeStatus;

  const AccountCard({
    Key key,
    this.cardModel,
    this.onChangeStatus,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const padding = 12.0;
    return Container(
      width: ScreenUtil.screenWidthDp * 3 / 4,
      height: AccountConstant.heightItemCard,
      decoration: BoxDecoration(
        color: AppColors.primaryColor,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Stack(
        children: [
          Positioned(
              top: padding,
              left: padding,
              child: FlutterLogo(
                size: 38,
              )),
          Positioned(
            top: 2,
            right: 2,
            child: Transform.scale(
              scale: 0.7,
              child: CupertinoSwitch(
                onChanged: (value) {
                  onChangeStatus(cardModel);
                },
                value: cardModel.enable,
                trackColor: AppColors.grey3,
              ),
            ),
          ),
          Positioned(
            bottom: padding,
            left: padding,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'ID ${cardModel.id}',
                  style: AppTextTheme.normalWhite.copyWith(
                    fontSize: 12,
                  ),
                ),
                Text(
                  '${cardModel.place}',
                  style: AppTextTheme.bold14PxW700
                      .copyWith(color: AppColors.white),
                ),
              ],
            ),
          ),
          Positioned(
              right: padding,
              bottom: padding,
              child: Text(
                '${cardModel.name}',
                style: AppTextTheme.bold16PxW700
                    .copyWith(color: AppColors.white, fontSize: 18),
              ))
        ],
      ),
    );
  }
}
