import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/presentation/journey/feature/account/account_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/circular_percent_indicator.dart';
import 'package:smart_building/presentation/widgets/custom_common_button.dart';

class AccountCompleteInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              translate(AccountConstant.completeInfo),
              style: AppTextTheme.bold16PxW700,
            ),
            const SizedBox(height: 12),
            Container(
              width: ScreenUtil.screenWidthDp * 3 / 4,
              padding: EdgeInsets.all(12),
              decoration: BoxDecoration(
                  gradient:
                      LinearGradient(colors: CommonConstant.gradientPrimary),
                  borderRadius: BorderRadius.circular(12)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      SvgPicture.asset(
                        IconConstant.person,
                        width: 14,
                        height: 14,
                      ),
                      const SizedBox(width: 4),
                      Text(
                        translate(AccountConstant.yourInfo),
                        style: AppTextTheme.normalWhite.copyWith(
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const Spacer(),
                      SvgPicture.asset(IconConstant.right)
                    ],
                  ),
                  const SizedBox(height: 6),
                  Text(
                    translate(AccountConstant.provideEnoughInfo),
                    style: AppTextTheme.normalWhite.copyWith(fontSize: 12),
                  ),
                  Row(
                    children: [
                      const Spacer(),
                      CustomCirclePercent(
                        percent: 75,
                        size: 45,
                        color: AppColors.white,
                        backgroundColor: Color(0xffA096FF),
                      ),
                    ],
                  ),
                  const SizedBox(height: 6),
                  Row(
                    children: [
                      Text(
                        translate(AccountConstant.verifyEmail),
                        style: AppTextTheme.normalWhite.copyWith(fontSize: 12),
                      ),
                      const Spacer(),
                      Text(
                        '7.5/10',
                        style: AppTextTheme.medium.copyWith(fontSize: 14),
                      )
                    ],
                  )
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
