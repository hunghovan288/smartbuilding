import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/data/models/card_model.dart';
import 'package:smart_building/presentation/journey/feature/account/account_constant.dart';
import 'package:smart_building/presentation/journey/feature/account/widget/account_card.dart';
import 'package:smart_building/presentation/journey/feature/account/widget/account_complete_info.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_common_button.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class AccountScreen extends StatefulWidget {
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  List<CardModel> _cardsModel;

  @override
  void initState() {
    _cardsModel = MockConstant.mockCards;
    super.initState();
  }

  void _onChangeStatus(CardModel cardModel) {
    for (var i = 0; i < _cardsModel.length; i++) {
      if (_cardsModel[i].id == cardModel.id) {
        _cardsModel[i].enable = !_cardsModel[i].enable;
        break;
      }
    }
    setState(() {});
  }
  void _onLogOut(){
    Routes.instance.navigateAndRemove(RouteName.loginScreen);
  }
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        onBack: () {
          Routes.instance.pop();
        },
        title: translate(AccountConstant.account),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 12),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: PixelConstant.marginHorizontal),
            child: Text(
              translate(AccountConstant.cardManager),
              style: AppTextTheme.bold16PxW700,
            ),
          ),
          const SizedBox(height: 12),
          CarouselSlider(
            options: CarouselOptions(
                autoPlay: false,
                aspectRatio: 2.0,

                enlargeCenterPage: true,
                enableInfiniteScroll: false),
            items: _cardsModel
                .map((e) => AccountCard(
                      cardModel: e,
                      onChangeStatus: _onChangeStatus,
                    ))
                .toList(),
          ),
          const SizedBox(height: 16),
          AccountCompleteInfo(),

          CustomCommonButton(
            text: translate(AccountConstant.logout),
            padding:PixelConstant.marginHorizontal ,
            onTap: _onLogOut,
          ),
          SizedBox(height: ScreenUtil.bottomBarHeight + 20),
        ],
      ),
    );
  }
}
