import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/global.dart';
import 'package:smart_building/common/bloc/snackbar_bloc/snackbar_bloc.dart';
import 'package:smart_building/common/bloc/snackbar_bloc/snackbar_event.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/constant/image_constant.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/constant/string_constant.dart';
import 'package:smart_building/common/injector/injector.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/presentation/journey/container/container_constant.dart';
import 'package:smart_building/presentation/journey/feature/pay_success/pay_success_constant.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_common_button.dart';
import 'package:smart_building/presentation/widgets/custom_image_network.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';
import 'package:smart_building/presentation/widgets/snackbar_widget/enum.dart';

class PaySuccessScreen extends StatefulWidget {
  @override
  _PaySuccessScreenState createState() => _PaySuccessScreenState();
}

class _PaySuccessScreenState extends State<PaySuccessScreen> {
  void _onPrintBill() {
    Injector.resolve<SnackBarBloc>().add(ShowSnackbarEvent(
        content: translate(StringConstant.comingSoon),
        type: SnackBarType.noStatus));
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: Stack(
        children: [
          Image.asset(
            ImageConstant.paySuccessBackground3,
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomAppBar(
                gradient: false,
                onBack: () {
                  Routes.instance.pop();
                },
                backgroundColor: Colors.transparent,
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    translate(
                      StringConstant.bill,
                    ),
                    style: AppTextTheme.bold16PxW700
                        .copyWith(color: AppColors.white, fontSize: 20),
                  ),
                ],
              ),
              const SizedBox(height: 33),
              Container(
                width: double.infinity,
                padding: EdgeInsets.all(16),
                margin: EdgeInsets.symmetric(
                    horizontal: PixelConstant.marginHorizontal),
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          IconConstant.paySuccessDone,
                          width: 74,
                          height: 74,
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          translate(PaySuccessConstant.payDone),
                          style:
                              AppTextTheme.bold16PxW700.copyWith(fontSize: 20),
                        ),
                      ],
                    ),
                    const SizedBox(height: 45),
                    Row(
                      children: [
                        Container(
                          child: CustomImageNetwork(
                            url:
                                'https://thethaiger.com/wp-content/uploads/2020/04/hoang-thuy-linh-1-1150-1586055948.jpg',
                            width: 52,
                            height: 52,
                            fit: BoxFit.cover,
                            border: 26,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(
                                width: 3, color: AppColors.primaryColor),
                            shape: BoxShape.circle,
                          ),
                        ),
                        const SizedBox(width: 12),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              translate(PaySuccessConstant.resident),
                              style: AppTextTheme.boldGrey12Sp,
                            ),
                            Text(
                              'Thuy Linh',
                              style: AppTextTheme.bold16PxW700,
                            )
                          ],
                        ))
                      ],
                    ),
                    const SizedBox(height: 20),
                    Text(
                      translate(PaySuccessConstant.billNumber),
                      style: AppTextTheme.boldGrey12Sp,
                    ),
                    Text(
                      '#D04689564556',
                      style: AppTextTheme.bold16PxW700,
                    ),
                    const SizedBox(height: 16),
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              Text(
                                translate(PaySuccessConstant.money),
                                style: AppTextTheme.boldGrey12Sp,
                              ),
                              Text(
                                '200.000',
                                style: AppTextTheme.bold16PxW700,
                              ),
                            ],
                            crossAxisAlignment: CrossAxisAlignment.start,
                          ),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                translate(PaySuccessConstant.fee),
                                style: AppTextTheme.boldGrey12Sp,
                              ),
                              Text(
                                '0.00',
                                style: AppTextTheme.bold16PxW700,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 30),
                    CustomCommonButton(
                      text: translate(PaySuccessConstant.invoicePrinting),
                      padding: 30,
                      onTap: _onPrintBill,
                    ),
                  ],
                ),
              )
            ],
          ),
          Positioned(
            top: ScreenUtil.screenHeightDp * 1 / 3,
            left: 0,
            child: _centerCircle(),
          ),
          Positioned(
              top: ScreenUtil.screenHeightDp * 1 / 3,
              right: 0,
              child: _centerCircle()),
        ],
      ),
    );
  }

  Widget _centerCircle() => Container(
        width: 32,
        height: 32,
        decoration: BoxDecoration(
            color: AppColors.primaryColor, shape: BoxShape.circle),
      );
}
