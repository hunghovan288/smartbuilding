class PaySuccessConstant{
  static const String _path = 'PaySuccessConstant.';
  static const String payDone = '${_path}payDone';
  static const String billNumber = '${_path}billNumber';
  static const String money = '${_path}money';
  static const String fee = '${_path}fee';
  static const String invoicePrinting = '${_path}invoicePrinting';
  static const String resident = '${_path}resident';
}