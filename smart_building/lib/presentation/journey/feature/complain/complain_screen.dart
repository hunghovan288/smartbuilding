import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/utils/common_util.dart';
import 'package:smart_building/presentation/journey/container/container_constant.dart';
import 'package:smart_building/presentation/journey/feature/complain/complain_constant.dart';
import 'package:smart_building/presentation/journey/feature/complain/widgets/complain_add.dart';
import 'package:smart_building/presentation/journey/feature/complain/widgets/complain_widget_item.dart';
import 'package:smart_building/presentation/journey/feature/create_complain/create_complain_screen.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class ComplainScreen extends StatefulWidget {
  @override
  _ComplainScreenState createState() => _ComplainScreenState();
}

class _ComplainScreenState extends State<ComplainScreen> {
  void _onCreateComplain() {
    CommonUtil.showCustomBottomSheet(
        context: context, child: CreateComplainBottomSheet());
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      backgroundColor: AppColors.backgroundCommon,
      customAppBar: CustomAppBar(
        title: translate(ContainerConstant.complain),
      ),
      body: ListView(
        padding: EdgeInsets.only(top: 12),
        children: [
          ComplainAddWidget(
            onTap: _onCreateComplain,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: PixelConstant.marginHorizontal, vertical: 12),
            child: Text(
              translate(ComplainConstant.complainHistory),
              style: AppTextTheme.bold16PxW700,
            ),
          ),
          Column(
            children: MockConstant.mockComplain
                .map((e) => Padding(
                      padding: const EdgeInsets.only(bottom: 12),
                      child: ComplainWidgetItem(
                        complainModel: e,
                      ),
                    ))
                .toList(),
          )
        ],
      ),
    );
  }
}
