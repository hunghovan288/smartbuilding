import 'package:flutter/material.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/data/models/complain_model.dart';
import 'package:smart_building/presentation/journey/feature/complain/complain_util.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class ComplainWidgetItem extends StatelessWidget {
  final ComplainModel complainModel;
  final Function onTap;

  const ComplainWidgetItem({
    Key key,
    this.complainModel,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(12),
      margin: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
      height: DashBoardConstant.heightItemWidget + 5,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: AppColors.primaryColor,
        gradient: LinearGradient(
          colors: CommonConstant.gradientPrimary,
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                complainModel.title ?? '',
                style:
                    AppTextTheme.bold16PxW700.copyWith(color: AppColors.white),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              const SizedBox(height: 6),
              Text(
                complainModel.date ?? '',
                style: AppTextTheme.normalWhite.copyWith(fontSize: 12),
              )
            ],
          )),
          Container(
            width: 30,
            height: 30,
            decoration: BoxDecoration(
              color: AppColors.white,
              shape: BoxShape.circle,
            ),
            child: Center(
              child: ComplainUtil.mapStatusComplainToIcon(complainModel.status),
            ),
          )
        ],
      ),
    );
  }
}
