import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/presentation/journey/feature/complain/complain_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class ComplainAddWidget extends StatelessWidget {
  final Function onTap;

  const ComplainAddWidget({Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: ScreenUtil.screenWidthDp * 2 / 3,
        padding: EdgeInsets.all(12),
        margin: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
        height: CommonConstant.heightItemWidget66,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: AppColors.primaryColor,
          gradient: LinearGradient(
            colors: CommonConstant.gradientPrimary,
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
                child: Text(
              translate(
                ComplainConstant.addComplain,
              ),
              style: AppTextTheme.bold16PxW700.copyWith(color: AppColors.white),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            )),
            SvgPicture.asset(
              IconConstant.complainAdd,
              width: 34,
              height: 34,
            )
          ],
        ),
      ),
    );
  }
}
