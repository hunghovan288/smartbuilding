import 'package:flutter/material.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';

class ComplainUtil {


  static Widget mapStatusComplainToIcon(int status) {
    if (status == 1) {
      return Icon(
        Icons.check,
        size: 22,
        color: AppColors.green,
      );
    }
    return Icon(
      Icons.more_horiz_outlined,
      size: 22,
      color: AppColors.yellow,
    );
  }
}
