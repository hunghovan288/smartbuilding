import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/widgets/dashboard_upcoming_event/dashboard_upcoming_event_item.dart';
import 'package:smart_building/presentation/journey/feature/upcoming_event/upcoming_constant.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class UpcomingEventScreen extends StatefulWidget {
  @override
  _UpcomingEventScreenState createState() => _UpcomingEventScreenState();
}

class _UpcomingEventScreenState extends State<UpcomingEventScreen> {
  void _onBack() {
    Routes.instance.pop();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        title: translate(UpcomingConstant.upcomingEvent),
        onBack: _onBack,
      ),
      body: ListView(
        padding: EdgeInsets.only(top: 12,bottom: 12,left: PixelConstant.marginHorizontal),
        children: MockConstant.mockEvent
            .map((e) => Padding(
              padding: const EdgeInsets.only(bottom: 12),
              child: DashBoardUpcomingEventItem(eventModel: e,),
            ))
            .toList(),
      ),
    );
  }
}
