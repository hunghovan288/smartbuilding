import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/data/models/service_model.dart';
import 'package:smart_building/presentation/journey/feature/service_detail/widgets/service_detail_widget_item.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class ServiceDetailScreen extends StatefulWidget {
  final ServiceModel serviceModel;

  const ServiceDetailScreen({Key key, this.serviceModel}) : super(key: key);

  @override
  _ServiceDetailScreenState createState() => _ServiceDetailScreenState();
}

class _ServiceDetailScreenState extends State<ServiceDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        title: translate(widget.serviceModel.text) ?? '',
        onBack: () {
          Routes.instance.pop();
        },
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(
          horizontal: PixelConstant.marginHorizontal,
          vertical: 16,
        ),
        children: [
          ServiceDetailWidgetItem(),
          ServiceDetailWidgetItem(),
          ServiceDetailWidgetItem(),
          ServiceDetailWidgetItem(),
          ServiceDetailWidgetItem(),
          ServiceDetailWidgetItem(),
          ServiceDetailWidgetItem(),
        ],
      ),
    );
  }
}
