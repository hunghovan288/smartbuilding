import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/presentation/journey/feature/service_detail/service_detail_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class ServiceDetailWidgetItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 117,
      margin: EdgeInsets.only(bottom: 12),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: CommonConstant.gradientPrimary,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: [
                Text(
                  'Don nha theo gio',
                  style: AppTextTheme.w500Size18White,
                ),
                const Spacer(),
                SvgPicture.asset(
                  IconConstant.serviceDetailLock,
                  width: 20,
                  height: 20,
                ),
                const SizedBox(width: 4),
                Text(
                  '2H',
                  style: AppTextTheme.normalWhite
                      .copyWith(color: AppColors.yellow, fontSize: 16),
                )
              ],
            ),
          ),
          const Spacer(),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Container(
                  height: 27,
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.circular(14)),
                  child: Center(
                    child: Text(
                      translate(ServiceDetailConstant.sendRequest),
                      style: AppTextTheme.normalWhite.copyWith(
                        color: AppColors.primaryColor,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
              ),
              const Spacer(),
              Padding(
                padding: const EdgeInsets.only(right: 12),
                child: Text(
                  '300.000đ',
                  style: AppTextTheme.w500Size18White.copyWith(fontSize: 16),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
