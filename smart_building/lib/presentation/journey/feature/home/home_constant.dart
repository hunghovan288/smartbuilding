class HomeConstant{
  static const String _path ='HomeConstant.';
  static const String mon ='${_path}mon';
  static const String tus ='${_path}tus';
  static const String wed ='${_path}wed';
  static const String thu ='${_path}thu';
  static const String fri ='${_path}fri';
  static const String sat ='${_path}sat';
  static const String sun ='${_path}sun';

  static const String jan='${_path}jan';
  static const String Feb='${_path}Feb';
  static const String Mar='${_path}Mar';
  static const String Apr='${_path}Apr';
  static const String May='${_path}May';
  static const String Jun='${_path}Jun';
  static const String Jul='${_path}Jul';
  static const String Aug='${_path}Aug';
  static const String Sep='${_path}Sep';
  static const String Oct='${_path}Oct';
  static const String Nov='${_path}Nov';
  static const String Dec='${_path}Dec';

  static const String day='${_path}day';
  static const String week='${_path}week';
  static const String month='${_path}month';
  static const String Year='${_path}year';

  static const String myHome='${_path}myHome';
  static const String electricalPower='${_path}electricalPower';
  static const String temperature='${_path}temperature';
  static const String humidity='${_path}humidity';
  static const String otherRoom='${_path}otherRoom';
  static const String all='${_path}all';
  static const String roomEquipment='${_path}roomEquipment';

}