import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/data/models/room_model.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/custom_image_network.dart';

class HomeOtherRoomItem extends StatelessWidget {
  final RoomModel roomModel;

  const HomeOtherRoomItem({Key key, this.roomModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 193,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: AppColors.white,
        boxShadow: CommonConstant.defaultShadow,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              CustomImageNetwork(
                url: roomModel.url,
                width: double.infinity,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(12),
                    topLeft: Radius.circular(12)),
                height: 111,
                fit: BoxFit.cover,
              ),
              Positioned(
                top: 12,
                right: 12,
                child: Container(
                  decoration: BoxDecoration(
                    color: AppColors.black.withOpacity(0.7),
                    borderRadius: BorderRadius.circular(6),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 12,vertical: 4),
                  child: Center(
                    child: Text(
                      'Chiều',
                      style: AppTextTheme.bold14PxW700.copyWith(
                        color: AppColors.white,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 16),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: PixelConstant.marginHorizontal,
            ),
            child: Text(
              roomModel.name ?? '',
              style: AppTextTheme.bold16PxW700,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          const SizedBox(height: 4),
          Row(
            children: [
              const Spacer(),
              Container(
                width: 20,
                height: 20,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: AppColors.primaryColor),
                child: Center(
                  child: SvgPicture.asset(
                    IconConstant.homeLight,
                    width: 12,
                    height: 12,
                    color: AppColors.white,
                  ),
                ),
              ),
              const SizedBox(width: 4),
              Text(
                '+3',
                style: AppTextTheme.smallGrey
                    .copyWith(color: AppColors.primaryColorText),
              ),
              const SizedBox(width: PixelConstant.marginHorizontal),
            ],
          )
        ],
      ),
    );
  }
}
