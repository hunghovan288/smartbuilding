import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/data/models/room_model.dart';
import 'package:smart_building/presentation/journey/feature/home/home_constant.dart';
import 'package:smart_building/presentation/journey/feature/home/widgets/home_other_room/home_other_room_item.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class HomeOtherRoom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: [
          const SizedBox(height: 12),
          Row(
            children: [
              const SizedBox(width: PixelConstant.marginHorizontal,),
              Text(
                translate(HomeConstant.otherRoom),
                style: AppTextTheme.bold16PxW700.copyWith(
                  fontSize: 15,
                  color: AppColors.grey7,
                ),
              ),
              const Spacer(),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  children: [
                    Text(translate(HomeConstant.all),
                        style: AppTextTheme.bold14PxW700.copyWith(
                          color: Color(0xff14BBB1),
                        )),
                    SvgPicture.asset(IconConstant.homeRightAll)
                  ],
                ),
              ),

            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
            child: Row(
              children: [
                Expanded(
                  child: HomeOtherRoomItem(
                    roomModel: MockConstant.mockRooms[0],
                  ),
                ),
                const SizedBox(width: 12),
                Expanded(
                  child: HomeOtherRoomItem(
                    roomModel: MockConstant.mockRooms[1],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
