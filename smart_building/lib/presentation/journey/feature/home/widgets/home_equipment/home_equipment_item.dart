import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/data/models/equipment_model.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class HomeEquipmentThreeItem extends StatelessWidget {
  final List<EquipmentModel> equipmentModels;

  const HomeEquipmentThreeItem({Key key, this.equipmentModels})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if(equipmentModels.length==1){
      equipmentModels..add(null)..add(null);
    }
    if(equipmentModels.length==2){
      equipmentModels.add(null);
    }
    return Row(
      children: equipmentModels
          .map((e) => Expanded(
                  child: Padding(
                padding: EdgeInsets.only(
                  right: 12,
                ),
                child:e!=null? HomeEquipmentItem(
                  equipmentModel: e,
                ):const SizedBox(),
              )))
          .toList(),
    );
  }
}

class HomeEquipmentItem extends StatelessWidget {
  final EquipmentModel equipmentModel;

  const HomeEquipmentItem({Key key, this.equipmentModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: CommonConstant.defaultShadow,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              const Spacer(),
              Container(
                width: 6,
                height: 6,
                decoration: BoxDecoration(
                    color: AppColors.primaryColor, shape: BoxShape.circle),
              )
            ],
          ),
          SvgPicture.asset(
            IconConstant.homeLight,
            width: 24,
            height: 24,
            color: AppColors.green,
          ),
          const SizedBox(height: 8),
          Text(
            equipmentModel.name ?? '',
            style: AppTextTheme.bold14PxW700
                .copyWith(color: AppColors.primaryColorText),
          ),
          Text(
            '80% brightness',
            style: AppTextTheme.normalWhite.copyWith(
              color: AppColors.primaryColorText,
              fontSize: 12,
            ),
          )
        ],
      ),
    );
  }
}
