import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/mock_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/data/models/equipment_model.dart';
import 'package:smart_building/presentation/journey/feature/home/home_constant.dart';
import 'package:smart_building/presentation/journey/feature/home/widgets/home_equipment/home_equipment_item.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class HomeEquipment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<List<EquipmentModel>> listEquipment = [];
    for (int i = 0; i < MockConstant.mockEquipment.length; i += 3) {
      if (i + 3 >= MockConstant.mockEquipment.length) {
        listEquipment.add(MockConstant.mockEquipment
            .sublist(i, MockConstant.mockEquipment.length));
        break;
      }
      listEquipment.add(MockConstant.mockEquipment.sublist(i, i + 3));
    }
    return Container(
      width: double.infinity,
      child: Column(
        children: [
          Row(
            children: [
              const SizedBox(width:PixelConstant.marginHorizontal ,),
              Text(
                translate(HomeConstant.roomEquipment),
                style: AppTextTheme.bold16PxW700.copyWith(
                  fontSize: 15,
                  color: AppColors.grey7,
                ),
              ),
              const Spacer(),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Icon(
                  Icons.add,
                  size: 20,
                  color: AppColors.primaryColor,
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(left: PixelConstant.marginHorizontal,right: 4),
            child: Column(
              children: listEquipment
                  .map((e) => Padding(
                        padding: const EdgeInsets.only(bottom: 12),
                        child: HomeEquipmentThreeItem(
                          equipmentModels: e,
                        ),
                      ))
                  .toList(),
            ),
          ),
          const SizedBox(height: 12),
        ],
      ),
    );
  }
}
