import 'dart:math';

import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/data/models/local/column_chart_model.dart';

class ColumnChartUtil {
  static List<ArgumentDataColumnChart> mapDataToColumn(
      List<ColumnChartModel> dataColumn) {
    final result = <ArgumentDataColumnChart>[];
    if (dataColumn == null) {
      return null;
    }
    int timeType = CommonConstant.TYPE_TIME_WEEK;
    if (dataColumn.length == 12) {
      timeType = CommonConstant.TYPE_TIME_YEAR;
    } else if (dataColumn.length > 13) {
      timeType = CommonConstant.TYPE_TIME_MONTH;
    }
    for (final obj in dataColumn) {
      if (timeType == CommonConstant.TYPE_TIME_WEEK) {
        if (obj.textTime == 8) {
          result.add(
              ArgumentDataColumnChart(textTime: 'CN', maxValue: obj.total,id: obj.id));
        } else {
          result.add(ArgumentDataColumnChart(
              textTime: 'Thứ ${obj.textTime}', maxValue: obj.total,id: obj.id));
        }
      } else if (timeType == CommonConstant.TYPE_TIME_MONTH) {
        result.add(ArgumentDataColumnChart(
            textTime: 'Ngày ${obj.textTime}', maxValue: obj.total,id: obj.id));
      } else {
        result.add(ArgumentDataColumnChart(
            textTime: 'Tháng ${obj.textTime}', maxValue: obj.total,id: obj.id));
      }
    }
    return result;
  }
  static List<int> mapMaxTotalProductToListValueY(int input) {
    try {
      if (input == null || input.runtimeType != int) {
        return null;
      }
      if (input <= 10) {
        return [10, 8, 6, 4, 2, 0];
      }
      int maxValue = getMaValue(input);
      List<int> result = [];
      result.add(maxValue);
      for (int i = 1; i <= 5; i++) {
        result.add(maxValue - (maxValue ~/ 5) * i);
      }
      return result;
    } catch (e) {
      return [];
    }
  }

  // xác định nằm trong thang 5 hay 10
  static int getMaValue(int input) {
    if (input <= 10) {
      return 10;
    }
    if (input % pow(10, '$input'.length - 1) == 0) {
      return input;
    }
    if (int.parse('$input'.substring(0, 1)) < 5) {
      return 5 * pow(10, '$input'.length - 1);
    }
    return 10 * pow(10, '$input'.length - 1);
  }
}

class ArgumentDataColumnChart {
  final String textTime;
  final int maxValue;
  final int id;
  ArgumentDataColumnChart({this.textTime, this.maxValue,this.id});
}
