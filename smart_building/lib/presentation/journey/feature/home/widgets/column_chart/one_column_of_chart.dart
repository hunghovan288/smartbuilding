import 'package:flutter/material.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/presentation/journey/feature/home/widgets/column_chart/column_chart_util.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';

class OneColumnOfChart extends StatelessWidget {
  final int value;
  final int maxValue;
  final bool selected;

  const OneColumnOfChart({
    Key key,
    this.value,
    this.selected,
    this.maxValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
      width: 18,
      height: ((value ?? 0) / ColumnChartUtil.getMaValue(maxValue)) * 250,
      child: Column(
        children: [
          Expanded(
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: !selected
                      ? CommonConstant.gradientPrimary
                      : [Color(0xffFD0000), Color(0xffFFE007A)],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(9),
                  topRight: Radius.circular(9),
                ),
              ),
            ),
          ),
          const SizedBox(height: 8),
          !selected
              ? const SizedBox(
                  height: 3,
                )
              : Container(
                  width: double.infinity,
                  height: 3,
                  color: AppColors.endPurple,
                )
        ],
      ),
    );
  }
}
