import 'package:flutter/material.dart';
import 'package:smart_building/presentation/journey/feature/home/widgets/column_chart/column_chart_constant.dart';
import 'package:smart_building/presentation/journey/feature/home/widgets/column_chart/column_chart_util.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class BackGroundChart extends StatelessWidget {
  final EdgeInsets padding;
  final int maxValue;

  const BackGroundChart({Key key, this.padding, this.maxValue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: ColumnChartConstant.heightColumnChart,
      color: AppColors.white,
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '${ColumnChartUtil.getMaValue(maxValue)} Kw',
            style: AppTextTheme.smallGrey.copyWith(
              color: AppColors.primaryColor,
              fontWeight: FontWeight.w700,
            ),
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    children:
                        ColumnChartUtil.mapMaxTotalProductToListValueY(maxValue)
                            .map(
                              (e) => Container(
                                height: 50,
                                alignment: Alignment.topCenter,
                                padding: EdgeInsets.only(top: 10),
                                child: Divider(
                                  color: AppColors.grey4,
                                  height: 1,
                                ),
                              ),
                            )
                            .toList(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
