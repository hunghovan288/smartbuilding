import 'package:flutter/material.dart';
import 'package:smart_building/common/utils/log_util.dart';
import 'package:smart_building/data/models/local/column_chart_model.dart';
import 'package:smart_building/presentation/journey/feature/home/widgets/column_chart/background.dart';
import 'package:smart_building/presentation/journey/feature/home/widgets/column_chart/column_chart_constant.dart';
import 'package:smart_building/presentation/journey/feature/home/widgets/column_chart/column_chart_util.dart';
import 'package:smart_building/presentation/journey/feature/home/widgets/column_chart/one_column_of_chart.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class ColumnChart extends StatefulWidget {
  final int timeType;

  const ColumnChart({Key key, this.timeType}) : super(key: key);

  @override
  _ColumnChartState createState() => _ColumnChartState();
}

class _ColumnChartState extends State<ColumnChart> {
  int maxValueOfY = 0;
  List<ArgumentDataColumnChart> _dataChart;
  int _idColumnSelected;

  @override
  void initState() {
    _mapData([
      ColumnChartModel(textTime: 2, total: 6, id: 1),
      ColumnChartModel(textTime: 3, total: 9, id: 2),
      ColumnChartModel(textTime: 4, total: 2, id: 3),
      ColumnChartModel(textTime: 5, total: 3, id: 4),
      ColumnChartModel(textTime: 6, total: 2, id: 5),
      ColumnChartModel(textTime: 7, total: 1, id: 6),
      ColumnChartModel(textTime: 8, total: 5, id: 7),
    ]);
    super.initState();
  }

  void _mapData(List<ColumnChartModel> dataColumns) {
    _dataChart = ColumnChartUtil.mapDataToColumn(dataColumns);
    if (_dataChart != null) {
      for (final obj in _dataChart) {
        if (maxValueOfY < (obj?.maxValue ?? 0)) {
          maxValueOfY = obj.maxValue;
        }
      }
    }
    _idColumnSelected = _dataChart[0].id;
    LOG.d('_idColumnSelected: $_idColumnSelected');
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _dataChart == null
              ? SizedBox()
              : Stack(
                  children: [
                    BackGroundChart(
                      maxValue: maxValueOfY,
                    ),
                    Positioned(
                      bottom: 24,
                      left: 8,
                      right: 0,
                      child: Container(
                        width: double.infinity,
                        height: ColumnChartConstant.heightColumnChart,
                        child: Column(
                          children: [
                            Expanded(
                              child: ListView.builder(
                                itemBuilder: (context, index) {
                                  return _itemWidgetListView(
                                    time: _dataChart[index].textTime,
                                    maxTotalProduct: maxValueOfY,
                                    productOut: _dataChart[index].maxValue,
                                    idColumn: _dataChart[index].id,
                                    idColumnSelected: _idColumnSelected,
                                  );
                                },
                                itemCount: _dataChart.length,
                                scrollDirection: Axis.horizontal,
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
        ],
      ),
    );
  }

  Widget _itemWidgetListView(
      {String time,
      int productOut,
      int maxTotalProduct,
      int idColumn,
      int idColumnSelected}) {
    LOG.d('idColumnNe: $idColumn');
    LOG.d('idColumnSelected: $idColumnSelected');
    return InkWell(
      onTap: () {
        setState(() {
          _idColumnSelected = idColumn;
        });
      },
      child: Container(
        width: ColumnChartConstant.widthItemColumn,
        child: Column(
          children: [
            Spacer(),
            OneColumnOfChart(
              value: productOut,
              maxValue: maxTotalProduct,
              selected: idColumn == idColumnSelected,
            ),
            Container(
              height: 50,
              width: ColumnChartConstant.widthItemColumn,
              child: Center(
                  child: RotationTransition(
                child: Text(
                  time ?? '',
                  style: idColumn != idColumnSelected
                      ? AppTextTheme.smallGrey
                      : AppTextTheme.smallGrey.copyWith(
                          fontWeight: FontWeight.w500,
                          color: AppColors.black,
                        ),
                ),
                turns: AlwaysStoppedAnimation(315 / 360),
              )),
            ),
          ],
        ),
      ),
    );
  }
}
