import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/presentation/journey/feature/home/home_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';

class HomeInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
      child: Row(
        children: [
          Expanded(
            child: _info(
                title: translate(HomeConstant.electricalPower),
                icon: IconConstant.homeTemperature,
                info: '124',
                righWidget: Text(
                  'w',
                  style: AppTextTheme.bold14PxW700.copyWith(
                    color: AppColors.white,
                  ),
                )),
          ),
          const SizedBox(width: 12),
          Expanded(
            child: _info(
                title: translate(HomeConstant.temperature),
                icon: IconConstant.homeTemperature,
                info: '30',
                righWidget: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Container(
                      width: 6,
                      margin: EdgeInsets.only(top: 5,left: 2),
                      height: 6,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: AppColors.white,
                          width: 1,
                        ),
                        shape: BoxShape.circle
                      ),

                    ),
                    Text(
                      'C',
                      style: AppTextTheme.bold14PxW700.copyWith(
                        color: AppColors.white,
                      ),
                    ),
                  ],
                )),
          ),
          const SizedBox(width: 12),
          Expanded(
            child: _info(
                title: translate(HomeConstant.humidity),
                icon: IconConstant.homeHumidity,
                info: '80',
                righWidget: Text(
                  '%',
                  style: AppTextTheme.bold14PxW700.copyWith(
                    color: AppColors.white,
                  ),
                )),
          ),
        ],
      ),
    );
  }

  Widget _info({String title, String icon, String info, Widget righWidget}) =>
      Container(
          width: double.infinity,
          padding: EdgeInsets.only(left: 12),
          height: 96,
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: CommonConstant.gradientPrimary),
            borderRadius: BorderRadius.circular(12),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title ?? '',
                style: AppTextTheme.bold14PxW700
                    .copyWith(color: AppColors.white, fontSize: 10),
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  SvgPicture.asset(
                    icon,
                    width: 16,
                    height: 16,
                    color: AppColors.white,
                  ),
                  const SizedBox(width: 4),
                  Text(
                    info ?? '',
                    style: AppTextTheme.bold14PxW700
                        .copyWith(color: AppColors.white, fontSize: 20),
                  ),
                  righWidget
                ],
              )
            ],
          ));
}
