import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/presentation/journey/feature/home/home_constant.dart';
import 'package:smart_building/presentation/journey/feature/home/widgets/column_chart/column_chart.dart';
import 'package:smart_building/presentation/journey/feature/home/widgets/home_equipment/home_equipment.dart';
import 'package:smart_building/presentation/journey/feature/home/widgets/home_info.dart';
import 'package:smart_building/presentation/journey/feature/home/widgets/home_other_room/home_other_room.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/custom_appbar.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      autoDismissKeyboard: false,
      body: ListView(
        padding: EdgeInsets.only(top: 12),
        children: [
          SizedBox(height: ScreenUtil.statusBarHeight + 40),
          Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: PixelConstant.marginHorizontal),
            child: Text(translate(HomeConstant.myHome),
                style: AppTextTheme.bold16PxW700
                    .copyWith(color: AppColors.primaryColor, fontSize: 36)),
          ),
          ColumnChart(),
          HomeInfo(),
          HomeOtherRoom(),
          HomeEquipment(),
        ],
      ),
    );
  }
}
