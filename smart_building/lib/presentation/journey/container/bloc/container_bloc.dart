import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_building/presentation/journey/container/bloc/container_event.dart';
import 'package:smart_building/presentation/journey/container/bloc/container_state.dart';

class ContainerBloc extends Bloc<ContainerEvent, ContainerState> {
  ContainerBloc():super(ContainerInitState());

  @override
  Stream<ContainerState> mapEventToState(ContainerEvent event) async* {
    switch (event.runtimeType) {
      case ContainerMovePageEvent:
        yield ContainerMovePageState(event.index);
        break;
      case ContainerClickTabHomeEvent:
        yield ContainerClickTabHomeState();
        break;
    }
  }
}
