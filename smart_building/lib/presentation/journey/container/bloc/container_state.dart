abstract class ContainerState {}

class ContainerInitState extends ContainerState {}

class ContainerMovePageState extends ContainerState {
  final int pageIndex;

  ContainerMovePageState(this.pageIndex);
}

class ContainerClickTabCartState extends ContainerState {}

class ContainerClickTabHomeState extends ContainerState {}
