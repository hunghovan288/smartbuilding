abstract class ContainerEvent {
  final int index;

  ContainerEvent({this.index});
}

class ContainerMovePageEvent extends ContainerEvent {
  ContainerMovePageEvent(int index) : super(index: index);
}

class ContainerClickTabHomeEvent extends ContainerEvent {}
