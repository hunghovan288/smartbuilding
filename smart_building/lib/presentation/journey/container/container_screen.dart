import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_translate/global.dart';
import 'package:smart_building/common/constant/common_constant.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/injector/injector.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:flutter/material.dart';
import 'package:smart_building/common/utils/screen_utils.dart';
import 'package:smart_building/presentation/journey/container/bloc/container_bloc.dart';
import 'package:smart_building/presentation/journey/container/container_constant.dart';
import 'package:smart_building/presentation/journey/feature/bill/bill_screen.dart';
import 'package:smart_building/presentation/journey/feature/complain/complain_screen.dart';
import 'package:smart_building/presentation/journey/feature/dashboard/dashboard_screen.dart';
import 'package:smart_building/presentation/journey/feature/home/home_screen.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/bottom_nagivation_widget.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';
import 'package:smart_building/presentation/widgets/layout_contain_widget_keep_alive.dart';

class ScreenContainer extends StatelessWidget {
  List<String> icons = [
    'dashboard.svg',
    'complain.svg',
    'house.svg',
    'pay.svg',
  ];

  void _scanQr(){
    Routes.instance.navigateTo(RouteName.myQrScreen);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ContainerBloc>(
      create: (context) => Injector.resolve<ContainerBloc>(),
      child: CustomScaffold(
        body: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            BottomNavigation(
              icons: icons,
              tabViews: [
                LayoutContainWidgetKeepAlive(child: DashBoardScreen()),
                LayoutContainWidgetKeepAlive(child: ComplainScreen()),
                LayoutContainWidgetKeepAlive(child: HomeScreen()),
                LayoutContainWidgetKeepAlive(child: BillScreen()),
              ],
            ),
            _centerIconWidget(),
          ],
        ),
      ),
    );
  }

  Widget _centerIconWidget() {
    final screenWidget = ScreenUtil.screenWidthDp;
    return InkWell(
      onTap: _scanQr,
      child: Container(
        width: screenWidget / 5,
        height: 80,
        child: Column(
          children: [
            const SizedBox(height: 2),
            Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  gradient:
                      LinearGradient(colors: CommonConstant.gradientPrimary),
                  shape: BoxShape.circle),
              child: Center(
                child: SvgPicture.asset(
                  IconConstant.qrCode,
                  width: 30,
                  height: 30,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(height: 4),
            Text(
              translate(ContainerConstant.myCode),
              style: AppTextTheme.smallGrey.copyWith(
                  fontWeight: FontWeight.w700, color: AppColors.primaryColorText),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }
}
