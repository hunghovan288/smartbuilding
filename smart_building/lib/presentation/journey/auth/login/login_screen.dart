import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:smart_building/common/constant/icon_constant.dart';
import 'package:smart_building/common/constant/image_constant.dart';
import 'package:smart_building/common/constant/pixel_constant.dart';
import 'package:smart_building/common/extensions/screen_extension.dart';
import 'package:smart_building/common/navigation/route_names.dart';
import 'package:smart_building/presentation/journey/auth/login/login_constant.dart';
import 'package:smart_building/presentation/routes.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/themes/theme_text.dart';
import 'package:smart_building/presentation/widgets/custom_common_button.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';
import 'package:smart_building/presentation/widgets/custom_textfield.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  void _onLogin() {
    Routes.instance.navigateTo(RouteName.containerScreen);
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Image.asset(
            ImageConstant.backgroundSplash,
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: PixelConstant.marginHorizontal, vertical: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      translate(LoginConstant.hello),
                      style: AppTextTheme.title.copyWith(
                        color: AppColors.white,
                      ),
                    ),
                    const SizedBox(height: 8),
                    Text(
                      translate(LoginConstant.pleaseLogin),
                      style: AppTextTheme.smallGrey.copyWith(
                        color: AppColors.white,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                height: 585.h,
                decoration: BoxDecoration(
                    color: AppColors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15),
                    )),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: PixelConstant.marginHorizontal),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 24),
                      Text(
                        translate(LoginConstant.userName),
                        style: AppTextTheme.normalPrimary,
                      ),
                      SizedBox(
                        width: double.infinity,
                        child: CustomTextField(
                          prefixIcon: Padding(
                            padding: const EdgeInsets.only(
                                top: 12, bottom: 8, left: 4, right: 16),
                            child: SvgPicture.asset(
                              IconConstant.profile,
                            ),
                          ),
                          style:
                              AppTextTheme.normalPrimary.copyWith(fontSize: 16),
                          textInputAction: TextInputAction.done,
                        ),
                      ),
                      const SizedBox(height: 30),
                      Text(
                        translate(LoginConstant.password),
                        style: AppTextTheme.normalPrimary,
                      ),
                      SizedBox(
                        width: double.infinity,
                        child: CustomTextField(
                          prefixIcon: Padding(
                            padding: const EdgeInsets.only(
                                top: 12, bottom: 8, left: 4, right: 16),
                            child: SvgPicture.asset(
                              IconConstant.lock,
                            ),
                          ),
                          style:
                              AppTextTheme.normalPrimary.copyWith(fontSize: 16),
                          textInputAction: TextInputAction.done,
                          isPassword: true,
                        ),
                      ),
                      const SizedBox(height: 40),
                      CustomCommonButton(
                        text: translate(LoginConstant.login),
                        onTap: _onLogin,
                      )
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
