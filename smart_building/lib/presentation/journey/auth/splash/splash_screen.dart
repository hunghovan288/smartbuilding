import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smart_building/common/constant/image_constant.dart';
import 'package:smart_building/presentation/themes/theme_color.dart';
import 'package:smart_building/presentation/widgets/custom_scafford.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      backgroundColor: AppColors.primaryColor,
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            color: AppColors.primaryColor,
          ),
          Image.asset(
            ImageConstant.paySuccessBackground3,
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ),
          // Container(
          //   width: double.infinity,
          //   height: double.infinity,
          //   child: Center(
          //
          //   ),
          // )
        ],
      ),
    );
  }
}
